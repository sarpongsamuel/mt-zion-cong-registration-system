<?php
require_once 'core/init.php';
include 'includes/head.php';
include 'includes/navigate.php';
include 'helpers.php';
?>

<?php 

 if(isset($_GET['edit'])){
    $edit_id = (int)$_GET['edit'];


   $show = "SELECT * FROM members WHERE id ='$edit_id'";
   $showRes = $db->query($show);
   $showQ= mysqli_fetch_assoc($showRes);

} ?> 


<?php
  $gen ="SELECT * FROM cgroups Where parent ='2'";
  $genQ =$db->query($gen);

  $intrest ="SELECT * FROM cgroups Where parent ='1'";
  $IntQ =$db->query($intrest);
 ?>


 <h1 class="text-center">Edit member</h1><hr>
 <br>

 <form class="" action="ver_update.php" method="post" enctype="multipart/form-data">
  <div class="container-fluid">
    <div class="row">
      <h2 class="description"><b>Personal Details</b></h2>
      <div class="col-md-4 col-md-offset-2">
        <label for=""><h4>Verify ID Type</h4></label>
        <select class="form-control" name="com_type">
          <option value="<?=$showQ['member_id'] ?>"><?=$showQ['member_id'] ?></option>
          <option value="MC">MC</option>
          <option value="MNC">MNC</option>
          <option value="FNC">FNC</option>
          <option value="FC">FC</option>          
        </select>
      </div>
      <div class="col-md-4 col-md-offset-1">
        <label for=""><h4></h4></label>
        <input type="hidden" name="id" class="form-control" value="<?=$showQ['id'] ?>" placeholder="lastname">
        <label for=""><h4>Membership Type</h4></label>
        <select class="form-control" name="mem_type">
          <option value="<?=$showQ['membership'] ?>"><?=$showQ['membership'] ?></option>
          <option value="Regular">Regular</option>
          <option value="Distant">Distant</option>         
        </select>
      </div>
    </div>
    <br>

    <div class="row">
      <div class="col-md-4 col-md-offset-2">
        <label for=""><h4>firstname</h4></label>
        <input type="text" class="form-control" name="firstname" value="<?=$showQ['firstname'] ?>" placeholder="firstname">
      </div>
      <div class="col-md-4 col-md-offset-1">
        <label for=""><h4>Lastname</h4></label>
        <input type="text" name="lastname" class="form-control" value="<?=$showQ['lastname'] ?>" placeholder="lastname">
      </div>
    </div>
    <br>

      <div class="row">
      <div class="col-md-4 col-md-offset-2">
        <label for=""><h4>Date Of Birth</h4></label>
        <input type="date" name="bdate" class="form-control" value="<?=$showQ['bdate'] ?>">
      </div>
      <div class="col-md-4 col-md-offset-1">
        <label for=""><h4>Gender</h4></label>
        <select class="form-control" name="gender" value="">
          <option value="<?=$showQ['gender'] ?>"><?=$showQ['gender'] ?></option>	
          <option value="male">Male</option>
          <option value="Female">Female</option>
        </select>
      </div>
      </div>
      <br>

      <div class="row">
        <div class="col-md-4 col-md-offset-2">
          <label for=""><h4>Occupation</h4></label>
          <select class="form-control" name="occupation">
            <?php
              $occu = "SELECT * FROM occupation";
              $occuQ = $db->query($occu);
             ?>
            <?php  while($occupa = mysqli_fetch_assoc($occuQ)): ?>
            <option value="<?=$showQ['occupation'] ?>"><?=$showQ['occupation'] ?></option>	
            <option value="<?=$occupa['occupation'] ?>"><?=$occupa['occupation'] ?></option>
          <?php  endwhile; ?>
          </select>
        </div>
        <div class="col-md-4 col-md-offset-1">
          <label for=""><h4>Personal Contact</h4></label>
          <input type="text" name="phone" class="form-control" value="<?=$showQ['telephone'] ?>" placeholder="0XXXXXXXXX">
        </div>
      </div>
      <br>

          <div class="row">
          <div class="col-md-4 col-md-offset-2">
          <label for=""><h4>Place of Work</h4></label>
          <input type="text" name="work" class="form-control" value="<?=$showQ['workplace'] ?>" placeholder="companyname-town">
        </div>
        <div class="col-md-4 col-md-offset-1">
          <label for=""><h4>Marital Status</h4></label>
          <select class="form-control" name="status">
            <?php
              $pos ="SELECT * FROM marital";
              $posQ=$db->query($pos);
             ?>
            <?php while($stat =mysqli_fetch_assoc($posQ)): ?>
            <option value="<?=$showQ['marital'] ?>"><?=$showQ['marital'] ?></option>	
            <option value="<?=$stat['status'] ?>"><?=$stat['status'] ?></option>
            <?php endwhile; ?>
          </select>
        </div>
      </div>
      <br>

      <div class="row">
      <div class="col-md-4 col-md-offset-2">
      <label for=""><h4>Contact Of Nearest Family Member</h4></label>
      <input type="text" name="fam_phone" class="form-control" value="<?=$showQ['fam_phone'] ?>" placeholder="0XXXXXXXXX">
    </div>
    <div class="col-md-4 col-md-offset-1">
      <label for=""><h4>Name of Nearest Family Member</h4></label>
      <input type="text" name="fam_name" class="form-control" value="<?=$showQ['fam_name'] ?>" placeholder="">
    </div>
  </div>
  <br>

  <div class="row">
  <div class="col-md-4 col-md-offset-2">
    <label for=""><h4>Residence of Nearest family member</h4></label>
    <input type="text" name="residence" class="form-control" value="<?=$showQ['residence'] ?>" placeholder="House Number -Region">
  </div>
  <div class="col-md-4 col-md-offset-1">
    <label for=""><h4>House Number</h4></label>
    <input type="text" name="house" class="form-control" value="<?=$showQ['residence'] ?>" placeholder="TE XXX">
  </div>
  </div>
  <br>

  <div class="row">
  <div class="col-md-4 col-md-offset-2">
    <label for=""><h4>Baptism Date</h4></label>
    <input type="date" name="baptism" class="form-control" value="<?=$showQ['baptism_date'] ?>">
  </div>
<div class="col-md-4 col-md-offset-1">
    <label for=""><h4>Confirmation Date</h4></label>
  <input type="date" name="confirmation" class="form-control" value="<?=$showQ['confirmation_date'] ?>" placeholder="">
</div>
</div>
<br>

<div class="row">
<div class="col-md-4 col-md-offset-2">
  <label for=""><h4>Nearest Landmark/Area/house#</h4></label>
<textarea name="address" rows="8" cols="10" class="form-control" placeholder="" value="<?=$showQ['address'] ?>"><?=$showQ['address'] ?></textarea>
</div>
</div>
<hr>
<!-- other details -->
    <div class="row">
      <h2 class="description"><b>Other Details</b></h2>
      <div class="col-md-4 col-md-offset-2">
        <label for=""><h4>1. Are you a Church Leader ?</h4></label><br>
        <select class="form-control" name="leader">
        <option value="<?=$showQ['church_leader'] ?>"><?=$showQ['church_leader'] ?></option>	
          <option value="yes">Yes</option>
          <option value="no">No</option>
        </select>
      </div>
      <div class="col-md-4 col-md-offset-1">
        <label for=""><h4>2. Which Generational Group do You belong ?</h4></label>
        <select class="form-control" name="mgroup">
        	<option value="<?= $showQ['gen_group']?>"><?= $showQ['gen_group'] ?></option>	
          <?php while($gene =mysqli_fetch_assoc($genQ)): ?>
          <option value="<?= $gene['all_groups'] ?>"><?= $gene['all_groups'] ?></option>
              <?php endwhile; ?>
          </select>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4 col-md-offset-2">
      <label for=""><h4>3. Which Interest Group do You belong ?</h4></label>
         <select class="form-control" name="sgroup">
       <option value="<?=$showQ['intrest_group'] ?>"><?=$showQ['intrest_group'] ?></option>
        <?php while($intre =mysqli_fetch_assoc($IntQ)): ?>
        <option value="<?=$intre['all_groups'] ?>"><?=$intre['all_groups'] ?></option>
        <?php endwhile; ?>
      </select>
    </div>
    <div class="col-md-4 col-md-offset-1">
      <label for=""><h4>4. Are You A Communicant? </h4></label>
      <select class="form-control" name="communicant">
      	<option value="<?=$showQ['communicant']?>"><?=$showQ['communicant']?></option>
        <option value="yes">Yes</option>
        <option value="no">No</option>
      </select>
    </div>
  </div>
    <br><br>
    <div class="row">
      <input type="submit" name="submit"  class="btn btn-primary btn-md col-md-offset-5  complete" value="Edit member">
      <a href="index.php" class="btn btn-warning btn-md">Cancel</a>
    </div>
  </div>
</form>
?>