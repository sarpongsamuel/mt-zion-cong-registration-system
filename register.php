
<?php require 'core/init.php'; ?>
<?php include 'includes/head.php'; ?>
<?php include 'includes/navigate.php'; ?>
<?php include 'helpers.php'; ?>
<h1 class="text-center">Register Member</h1><hr>

<br>


<?php
  $gen ="SELECT * FROM cgroups Where parent ='2'";
  $genQ =$db->query($gen);

  $intrest ="SELECT * FROM cgroups Where parent ='1'";
  $IntQ =$db->query($intrest);
 ?>

<form class="" action="verify.php" method="post" enctype="multipart/form-data">
  <div class="container-fluid">
    <div class="row">
      <h2 class="description"><b>Personal Details</b></h2>
      <div class="col-md-4 col-md-offset-2">
        <label for=""><h4>Verify ID Type</h4></label>
        <select class="form-control" name="com_type">
          <option value="MC">MC</option>
          <option value="MNC">MNC</option>
          <option value="FNC">FNC</option>
          <option value="FC">FC</option>
        </select>
      </div>
       <div class="col-md-4 col-md-offset-1">
        <label for=""><h4>Membership type</h4></label>
        <select class="form-control" name="mem_type">
          <option value="Regular">Regular</option>
          <option value="Distant">Distant</option>
        </select>
      </div> 
    </div>
    <br>

    <div class="row">
      <div class="col-md-4 col-md-offset-2">
        <label for=""><h4>firstname</h4></label>
        <input type="text" class="form-control" name="firstname" value="" placeholder="firstname">
      </div>
      <div class="col-md-4 col-md-offset-1">
        <label for=""><h4>Lastname</h4></label>
        <input type="text" name="lastname" class="form-control" value="" placeholder="lastname">
      </div>
    </div>
    <br>

      <div class="row">
      <div class="col-md-4 col-md-offset-2">
        <label for=""><h4>Date Of Birth</h4></label>
        <input type="date" name="bdate" class="form-control" value="">
      </div>
      <div class="col-md-4 col-md-offset-1">
        <label for=""><h4>Gender</h4></label>
        <select class="form-control" name="gender" value="">
          <option value="male">Male</option>
          <option value="Female">Female</option>
        </select>
      </div>
      </div>
      <br>

      <div class="row">
        <div class="col-md-4 col-md-offset-2">
          <label for=""><h4>Occupation</h4></label>
          <select class="form-control" name="occupation">
            <?php
              $occu = "SELECT * FROM occupation";
              $occuQ = $db->query($occu);
             ?>
            <?php  while($occupa = mysqli_fetch_assoc($occuQ)): ?>
            <option value="<?=$occupa['occupation'] ?>"><?=$occupa['occupation'] ?></option>
          <?php  endwhile; ?>
          </select>
        </div>
        <div class="col-md-4 col-md-offset-1">
          <label for=""><h4>Personal Contact</h4></label>
          <input type="text" name="phone" class="form-control" value="" placeholder="0XXXXXXXXX">
        </div>
      </div>
      <br>

          <div class="row">
          <div class="col-md-4 col-md-offset-2">
          <label for=""><h4>Place of Work</h4></label>
          <input type="text" name="work" class="form-control" value="" placeholder="companyname-town">
        </div>
        <div class="col-md-4 col-md-offset-1">
          <label for=""><h4>Marital Status</h4></label>
          <select class="form-control" name="status">
            <?php
              $pos ="SELECT * FROM marital";
              $posQ=$db->query($pos);
             ?>
            <?php while($stat =mysqli_fetch_assoc($posQ)): ?>
            <option value="<?=$stat['status'] ?>"><?=$stat['status'] ?></option>
            <?php endwhile; ?>
          </select>
        </div>
      </div>
      <br>

      <div class="row">
      <div class="col-md-4 col-md-offset-2">
      <label for=""><h4>Contact Of Nearest Family Member</h4></label>
      <input type="text" name="fam_phone" class="form-control" value="" placeholder="0XXXXXXXXX">
    </div>
    <div class="col-md-4 col-md-offset-1">
      <label for=""><h4>Name of Nearest Family Member</h4></label>
      <input type="text" name="fam_name" class="form-control" value="" placeholder="">
    </div>
  </div>
  <br>

  <div class="row">
  <div class="col-md-4 col-md-offset-2">
    <label for=""><h4>Residence of Nearest family member</h4></label>
    <input type="text" name="residence" class="form-control" value="" placeholder="House Number -Region">
  </div>
  <div class="col-md-4 col-md-offset-1">
    <label for=""><h4>House Number</h4></label>
    <input type="text" name="house" class="form-control" value="" placeholder="TE XXX">
  </div>
  </div>
  <br>

  <div class="row">
  <div class="col-md-4 col-md-offset-2">
    <label for=""><h4>Baptism Date</h4></label>
    <input type="date" name="baptism" class="form-control" value="">
  </div>
<div class="col-md-4 col-md-offset-1">
    <label for=""><h4>Confirmation Date</h4></label>
  <input type="date" name="confirmation" class="form-control" value="" placeholder="">
</div>
</div>
<br>

<div class="row">
<div class="col-md-4 col-md-offset-2">
  <label for=""><h4>Nearest Landmark/Area/house#</h4></label>
<textarea name="address" rows="8" cols="10" class="form-control" placeholder=""></textarea>
</div>
<!-- <div class="col-md-4 col-md-offset-1">
  <label for=""><h4>Nearest Family Member Address</h4></label>
<textarea name="fam_address" rows="8" cols="10" class="form-control" placeholder="family memberName and companyAddress"></textarea>
</div> -->
</div>
<hr>
<!-- other details -->
<?php

?>
    <div class="row">
      <h2 class="description"><b>Other Details</b></h2>
      <div class="col-md-4 col-md-offset-2">
        <label for=""><h4>1. Are you a Church Leader ?</h4></label><br>
        <select class="form-control" name="leader">
          <option value="yes">Yes</option>
          <option value="no">No</option>
        </select>
      </div>
      <div class="col-md-4 col-md-offset-1">
        <label for=""><h4>2. Which Generational Group do You belong ?</h4></label>
        <select class="form-control" name="mgroup">
          <?php while($gene =mysqli_fetch_assoc($genQ)): ?>
          <option value="<?= $gene['all_groups'] ?>"><?= $gene['all_groups'] ?></option>
              <?php endwhile; ?>
          </select>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4 col-md-offset-2">
      <label for=""><h4>3. Which Interest Group do You belong ?</h4></label>
      <select class="form-control" name="sgroup">
        <?php while($intre =mysqli_fetch_assoc($IntQ)): ?>
        <option value="<?=$intre['all_groups'] ?>"><?=$intre['all_groups'] ?></option>
            <?php endwhile; ?>
      </select>
    </div>
    <div class="col-md-4 col-md-offset-1">
      <label for=""><h4>4. Are You A Communicant? </h4></label>
      <select class="form-control" name="communicant">
        <option value="yes">Yes</option>
        <option value="no">No</option>
      </select>
    </div>
  </div>
    <br><br>
    <div class="row">
      <input type="submit" name="submit"  class="btn btn-primary btn-md col-md-offset-5  complete" value="Register member">
      <a href="index.php" class="btn btn-warning btn-md">Cancel</a>
    </div>
  </div>
</form>



<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="js/jQuery.js"></script>


<?php include 'includes/footer.php'; ?>
<script src="js/jquery.js"></script>
