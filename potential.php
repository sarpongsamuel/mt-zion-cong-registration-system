<?php
require_once 'core/init.php';
include 'includes/head.php';
 include 'includes/navigate.php';
 include 'helpers.php';

 ?>

 <h2 class="text-center top"><strong>Pending Graduations List</strong></h2>
 <br>
 <hr>
 <div class="container-padded all">

<!-- youth People guild -->
<?php
$displayY ="SELECT * FROM members WHERE gen_group LIKE '%Potential YPG'  AND deleted ='0'";
$ypg=$db->query($displayY);
$countY=mysqli_num_rows($ypg);
 ?>
<div class="row ypg">
  <h1 class="text-center head">Potential Young People's Gulid</h1><hr>
<div class="col-md-12">
  <h3 class="text-center">--showing <?=$countY; ?> member(s)</h3>
 <table class="table table-bordered table-condensed table-striped ">
     <thead><th>View</th><th>member ID</th><th>Firstname</th><th>Lastname</th><th>Date Of Birth</th><th>Gender</th><th>Occupation</th><th>Telephone</th><th>Baptism Date</th><th>Confirmation Date</th><th>Church leader</th><th>Generational Group</th><th>Intrest Group</th><th>Communicant</th></thead>
     <tbody>
       <?php while($ypgQ =mysqli_fetch_assoc($ypg)): ?>
         <tr>
           <td><a href="display.php?view=<?=$ypgQ['id']; ?>"class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-eye-open"></span></a></td>
           <td><?=$ypgQ['member_id'].$ypgQ['id'] ?></td>
           <td><?=$ypgQ['firstname'] ?></td>
           <td><?=$ypgQ['lastname'] ?></td>
           <td><?=$ypgQ['bdate'] ?></td>
           <td><?=$ypgQ['gender'] ?></td>
           <td><?=$ypgQ['occupation'] ?></td>
           <td><?=$ypgQ['telephone'] ?></td>
           <td><?=$ypgQ['baptism_date'] ?></td>
           <td><?=$ypgQ['confirmation_date'] ?></td>
           <td><?=$ypgQ['church_leader'] ?></td>
           <td><?=$ypgQ['gen_group'] ?></td>
           <td><?=$ypgQ['intrest_group'] ?></td>
           <td><?=$ypgQ['communicant'] ?></td>
           <!-- <td><a href="#" class="btn btn-primary"><span class="glyphicon glyphicon-tint"></span></a></td> -->

<!--            <?php

        $dateOfBirth = $ypgQ['bdate'];
        $today = date("Y-m-d");
        $diff = date_diff(date_create($dateOfBirth),date_create($today));
        echo $today;
        echo "age ".$diff->format('%y');


    ?>
 -->
         <?php endwhile; ?>
     </tbody>
 </table>
</div>
</div>

<!--  YAF-->

<?php
$displayYAF ="SELECT * FROM members WHERE gen_group LIKE '%Potential YAF%'  AND deleted ='0'";
$yafQ=$db->query($displayYAF);
$countM=mysqli_num_rows($yafQ);
 ?>
<div class="row yaf">
  <h1 class="text-center head">Potential Young Adults Fellowship</h1><hr>
<div class="col-md-12">
  <h3 class="text-center">--showing <?=$countM; ?> member(s)</h3>
 <table class="table table-bordered table-condensed table-striped ">
     <thead><th>View</th><th>member ID</th><th>Firstname</th><th>Lastname</th><th>Date Of Birth</th><th>Gender</th><th>Occupation</th><th>Telephone</th><th>Baptism Date</th><th>Confirmation Date</th><th>Church leader</th><th>Generational Group</th><th>Intrest Group</th><th>Communicant</th></thead>
     <tbody>
       <?php while($yaf =mysqli_fetch_assoc($yafQ)): ?>
         <tr>
           <td><a href="display.php?view=<?=$yaf['id']; ?>"class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-eye-open"></span></a></td>
           <td><?=$yaf['member_id'].$yaf['id'] ?></td>
           <td><?=$yaf['firstname'] ?></td>
           <td><?=$yaf['lastname'] ?></td>
           <td><?=$yaf['bdate'] ?></td>
           <td><?=$yaf['gender'] ?></td>
           <td><?=$yaf['occupation'] ?></td>
           <td><?=$yaf['telephone'] ?></td>
           <td><?=$yaf['baptism_date'] ?></td>
           <td><?=$yaf['confirmation_date'] ?></td>
           <td><?=$yaf['church_leader'] ?></td>
           <td><?=$yaf['gen_group'] ?></td>
           <td><?=$yaf['intrest_group'] ?></td>
           <td><?=$yaf['communicant'] ?></td>
         <?php endwhile; ?>
     </tbody>
 </table>
</div>
</div>

<!-- Womens Fellowship -->
<?php
$displayW ="SELECT * FROM members WHERE gen_group LIKE '%Potential WF%'  AND deleted ='0'";
$womenQ=$db->query($displayW);
$countW=mysqli_num_rows($womenQ);
 ?>
<div class="row women">
  <h1 class="text-center head">Potential Women Fellowship</h1><hr>
<div class="col-md-12">
  <h3 class="text-center">--showing <?=$countW; ?> member(s)</h3>
 <table class="table table-bordered table-condensed table-striped ">
     <thead><th>View</th><th>member ID</th><th>Firstname</th><th>Lastname</th><th>Date Of Birth</th><th>Gender</th><th>Occupation</th><th>Telephone</th><th>Baptism Date</th><th>Confirmation Date</th><th>Church leader</th><th>Generational Group</th><th>Intrest Group</th><th>Communicant</th></thead>
     <tbody>
       <?php while($women =mysqli_fetch_assoc($womenQ)): ?>
         <tr>
           <td><a href="display.php?view=<?=$women['id']; ?>"class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-eye-open"></span></a></td>
           <td><?=$women['member_id'].$women['id'] ?></td>
           <td><?=$women['firstname'] ?></td>
           <td><?=$women['lastname'] ?></td>
           <td><?=$women['bdate'] ?></td>
           <td><?=$women['gender'] ?></td>
           <td><?=$women['occupation'] ?></td>
           <td><?=$women['telephone'] ?></td>
           <td><?=$women['baptism_date'] ?></td>
           <td><?=$women['confirmation_date'] ?></td>
           <td><?=$women['church_leader'] ?></td>
           <td><?=$women['gen_group'] ?></td>
           <td><?=$women['intrest_group'] ?></td>
           <td><?=$women['communicant'] ?></td>
         <?php endwhile; ?>
     </tbody>
 </table>
</div>
</div>



<!-- mens Fellowship -->
<?php
$displayM ="SELECT * FROM members WHERE gen_group LIKE 'Potential MF%'  AND deleted ='0'";
$menQ=$db->query($displayM);
$countM=mysqli_num_rows($menQ);
 ?>
<div class="row men">
  <h1 class="text-center head">Potential Men Fellowship</h1><hr>
<div class="col-md-12">
  <h3 class="text-center">--showing <?=$countM; ?> member(s)</h3>
 <table class="table table-bordered table-condensed table-striped ">
     <thead><th>View</th><th>member ID</th><th>Firstname</th><th>Lastname</th><th>Date Of Birth</th><th>Gender</th><th>Occupation</th><th>Telephone</th><th>Baptism Date</th><th>Confirmation Date</th><th>Church leader</th><th>Generational Group</th><th>Intrest Group</th><th>Communicant</th></thead>
     <tbody>
       <?php while($men =mysqli_fetch_assoc($menQ)): ?>
         <tr>
           <td><a href="display.php?view=<?=$men['id']; ?>"class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-eye-open"></span></a></td>
           <td><?=$men['member_id'].$men['id'] ?></td>
           <td><?=$men['firstname'] ?></td>
           <td><?=$men['lastname'] ?></td>
           <td><?=$men['bdate'] ?></td>
           <td><?=$men['gender'] ?></td>
           <td><?=$men['occupation'] ?></td>
           <td><?=$men['telephone'] ?></td>
           <td><?=$men['baptism_date'] ?></td>
           <td><?=$men['confirmation_date'] ?></td>
           <td><?=$men['church_leader'] ?></td>
           <td><?=$men['gen_group'] ?></td>
           <td><?=$men['intrest_group'] ?></td>
           <td><?=$men['communicant'] ?></td>
         <?php endwhile; ?>
     </tbody>
 </table>
</div>
</div>
</div>

 <?php include 'includes/footer.php'; ?>
