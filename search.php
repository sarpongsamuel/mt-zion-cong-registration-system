<?php
require_once 'core/init.php';
include 'includes/head.php';
 include 'includes/navigate.php';
 include 'helpers.php';

 ?>
 <?php

 if (isset($_GET['delete']) && !empty($_GET['delete'])) {
   $delete_id=(int)$_GET['delete'];
   $delete_id=sanitize($delete_id);
   $sql="UPDATE members set deleted = 1  WHERE id='$delete_id'";
   $db->query($sql);
   header('Location: view.php');
 }

  ?>

  <?php 
    if(isset($_POST['submit'])){
      $search = sanitize($_POST['id']);


      $display ="SELECT * FROM members WHERE (id ='$search') OR (firstname like '%$search%') OR (lastname like '%$search%')";
      $disp=$db->query($display);
      $count = mysqli_num_rows($disp);

    }

  ?>
<div class="container">


 <h3 class="text-center">Search Result(s)   ------<?=$count ?></h3>
 <div class="clearfix"></div>
 <hr>
 </div>
<br><br><br>

 <table class="table table-bordered table-condensed table-striped">
     <thead><th></th><th>View</th><th>ID</th><th>Firstname</th><th>Lastname</th><th>Date Of Birth</th><th>Gender</th><th>Occupation</th><th>Telephone</th><th>House Number</th><th>Adrress</th>
       <th>Church leader</th><th>Generational Group</th><th>Intrest Group</th><th>Communicant</th></thead>
     <tbody>
       <?php while($view =mysqli_fetch_assoc($disp)): ?>
         <tr>
           <td>
             <a href="update.php?edit=<?=$view['id'] ?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-pencil"></span></a>
             <a href="view.php?delete=<?=$view['id'] ?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-remove"></span></a>
           </td>
            <td><a href="display.php?view=<?=$view['id']; ?>"class="btn btn-primary btn-sm">View</a></td>
           <td><?=$view['member_id']."0".$view['id']; ?></td>
           <td><?=$view['firstname'] ?></td>
           <td><?=$view['lastname'] ?></td>
           <td><?=$view['bdate'] ?></td>
           <td><?=$view['gender'] ?></td>
           <td><?=$view['occupation'] ?></td>
           <td><?=$view['telephone'] ?></td>
           <td><?=$view['house'] ?></td>
           <td><?=$view['address'] ?></td>
           <td><?=$view['church_leader'] ?></td>
           <td><?=$view['gen_group'] ?></td>
           <td><?=$view['intrest_group'] ?></td>
           <td><?=$view['communicant'] ?></td>
         <?php endwhile; ?>
     </tbody>
 </table



 <?php include 'includes/footer.php'; ?>
