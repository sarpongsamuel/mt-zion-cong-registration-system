<?php
require_once 'core/init.php';
include 'includes/head.php';
include 'includes/navigate.php';
include '../helpers.php';
?>

<?php 

 if(isset($_GET['edit'])){
    $edit_id = (int)$_GET['edit'];


   $show = "SELECT * FROM children WHERE id ='$edit_id'";
   $showRes = $db->query($show);
   $showQ= mysqli_fetch_assoc($showRes);

} ?> 
 <style type="text/css">
 	h1{
 		text-transform: uppercase;
 		font-family: sans-serif;
 		margin-top: 40px;
 	}
 </style>
<h1 class="text-center top">Edit Member</h1><hr><br><br>
<form class="" action="ver_edit.php" method="post" enctype="multipart/form-data">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4 col-md-offset-2">
        <label for=""><h4>firstname</h4></label>
        <input type="text" class="form-control" name="firstname" value="<?=$showQ['firstname']; ?>" placeholder="firstname">
      </div>
      <div class="col-md-4 col-md-offset-1">
        <label for=""><h4>Lastname</h4></label>
        <input type="text" name="lastname" class="form-control" value="<?=$showQ['lastname']; ?>" placeholder="lastname">
      </div>
    </div>
    <br>

      <div class="row">
      <div class="col-md-4 col-md-offset-2">
        <label for=""><h4>Date Of Birth</h4></label>
        <input type="date" name="bdate" class="form-control" value="<?=$showQ['bdate']; ?>">
      </div>
      <div class="col-md-4 col-md-offset-1">
        <label for=""><h4>Gender</h4></label>
        <select class="form-control" name="gender" value="">
          <option value="<?=$showQ['gender']; ?>"><?=$showQ['gender']; ?></option>
          <option value="male">Male</option>
          <option value="Female">Female</option>
        </select>
      </div>
      </div>
      <br>

    <div class="row">
       <div class="col-md-4 col-md-offset-2">
         <label for=""><h4>Baptism Date</h4></label>
         <input type="date" name="baptism" class="form-control" value="<?=$showQ['baptism_date'] ?>">
       </div>
   <div class="col-md-4 col-md-offset-1">
     <label for=""><h4>House Number</h4></label>
     <input type="text" name="house" class="form-control" value="<?=$showQ['house_no'] ?>" placeholder="TE XXX">
  </div>  
   </div>
<br>

      <div class="row">
      	<div class="col-md-4 col-md-offset-2">
      <label for=""><h4>Parent/Guardian Name</h4></label>
      <input type="text" name="parent_name" class="form-control" value="<?=$showQ['P_name'] ?>" placeholder="">
    </div>
      <div class="col-md-4 col-md-offset-1">
      <label for=""><h4>Parent/Guardian Telephone</h4></label>
      <input type="text" name="parent_phone" class="form-control" value="<?=$showQ['P_phone'] ?>" placeholder="0XXXXXXXXX">
    </div>
  </div>
  <br>
  
  <div class="row">
	<div class="col-md-4 col-md-offset-2">
    <label for=""><h4>Relation To Member</h4></label>
    <select name="relation" class="form-control">
    	<option value="<?=$showQ['relation'] ?>"><?=$showQ['relation'] ?></option>
    	<option value="Parent">Parent</option>
    	<option value="Guardian">Guardian</option>
    </select>
  </div>
<div class="col-md-4 col-md-offset-1">
  <label for=""><h4>Nearest Landmark/Area/house#</h4></label>
  <textarea name="address" rows="4" cols="10" class="form-control" placeholder="" value="<?=$showQ['address'] ?>"><?=$showQ['address'] ?></textarea>
</div>
</div>
<br>
  
<div class="row">	
  <div class="col-md-4 col-md-offset-2">
    <label for=""><h4></h4></label>
    <input type="hidden" name="id" class="form-control" value="<?=$showQ['id'] ?>">
  </div>
</div>
</div>
<hr>
<!-- other details -->
<?php

?>
    <div class="row">
      <input type="submit" name="submit"  class="btn btn-primary btn-md col-md-offset-5  complete" value="Edit member">
      <a href="../index.php" class="btn btn-warning btn-md">Cancel</a>
    </div>
  </div>
</form>

