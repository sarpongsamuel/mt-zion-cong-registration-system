<nav class="navbar navbar-inverse probootstrap-navbar ">
   <div class="container">
   <div class="navbar-header">
<div class="btn-more js-btn-more visible-xs">
      <a href="#"><i class="icon-dots-three-vertical "></i></a>
</div>
<button type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false" aria-controls="navbar">
       <span class="sr-only">Toggle navigation</span>
       <span class="icon-bar"></span>
       <span class="icon-bar"></span>
       <span class="icon-bar"></span>
</button>

<a class="navbar-brand" href="index.php" title="ProBootstrap:Enlight">Mountzion Members Database</a>
</div>
<div id="navbar-collapse" class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php">Dashboard</a></li>
            <li><a href="members.php">View Registered</a></li>
            <li><a href="archived.php">Archived Members</a></li>
            <li><a href="all_groups.php">Church Groups</a></li>
            <li class="dropdown">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">View Category<span class="caret"></span></a>
                 <ul class="dropdown-menu">
                   <li><a href="communicants.php">Communicants</a></li>
                   <li><a href="leaders.php">Church Leaders</a></li>
                    <li><a href="potential.php">Pending Graduations</a></li>
                    <li><a href="distant.php">Distant Members</a></li>
                    <li><a href="regular.php">Regular Members</a></li>
                 </ul>
             </li>
            <li class="dropdown">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">occupations sorting<span class="caret"></span></a>
                 <ul class="dropdown-menu">
                   <li><a href="students.php">Students</a></li>
                   <li><a href="retired.php">Retired Workers</a></li>
                    <li><a href="unemployed.php">Unemployed</a></li>
                    <li><a href="non_salaried.php">Non Salaried</a></li>
                    <li><a href="govt.php">Government Workers</a></li>
                    <li><a href="private.php">Salaried Private Workers</a></li>
                 </ul>
             </li>
          </ul>
</div>
</div>
</nav>
