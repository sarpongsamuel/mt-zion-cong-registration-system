-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 21, 2019 at 02:46 PM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mountzion`
--

-- --------------------------------------------------------

--
-- Table structure for table `cgroups`
--

CREATE TABLE `cgroups` (
  `id` int(11) NOT NULL,
  `all_groups` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cgroups`
--

INSERT INTO `cgroups` (`id`, `all_groups`, `parent`) VALUES
(1, 'Young Peoples Guild', 2),
(2, 'Junior Youth', 2),
(3, 'Young Adults Fellowship', 2),
(4, 'Women Fellowship', 2),
(5, 'Children Service', 2),
(6, 'Church Choir', 1),
(7, 'Singing And Praises Team', 1),
(8, 'Singing Band', 1),
(9, 'Ushering', 1),
(11, 'Prayer', 1),
(12, 'Men Fellowship', 2),
(13, '---', 1),
(14, 'BSPG', 1),
(15, 'Potential YPG', 2),
(16, 'Potential YAF', 2),
(17, 'Potential MF', 2),
(18, 'Potential WF', 2);

-- --------------------------------------------------------

--
-- Table structure for table `children`
--

CREATE TABLE `children` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bdate` date NOT NULL,
  `house_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `baptism_date` date NOT NULL,
  `P_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `P_phone` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `children`
--

INSERT INTO `children` (`id`, `firstname`, `lastname`, `gender`, `bdate`, `house_no`, `baptism_date`, `P_name`, `P_phone`, `address`, `relation`, `deleted`) VALUES
(1, 'alex nyarko', 'asumadu', 'male', '2018-07-19', 'te 456', '2018-07-27', 'evelyn koranten', 242150301, 'mmm', 'Parent', 0);

-- --------------------------------------------------------

--
-- Table structure for table `junior`
--

CREATE TABLE `junior` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bdate` date NOT NULL,
  `house_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ver_baptism` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ver_confirm` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `confirmation_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `baptism_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `P_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `P_phone` int(11) NOT NULL,
  `academic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `school` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_church` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` int(11) NOT NULL,
  `age` int(11) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `junior`
--

INSERT INTO `junior` (`id`, `firstname`, `lastname`, `gender`, `bdate`, `house_no`, `ver_baptism`, `ver_confirm`, `confirmation_date`, `baptism_date`, `P_name`, `P_phone`, `academic`, `school`, `relation`, `parent_church`, `address`, `telephone`, `age`, `deleted`) VALUES
(1, 'VERA', 'NARTEY', 'Female', '2002-07-27', 'TF 75', 'Yes', 'No', '00/00/0000', '00/00/0000', 'MRS.EMELIA NARTEY', 548611995, 'SHS', 'MAKROSEC,GENERAL ARTS', 'Parent', 'Yes', 'NOPRAS FM,MID TAFO NEW ROAD', 266650572, 16, 0),
(2, 'RICHARD', 'ENYONAM', 'male', '2000-09-19', 'TE 229', 'Yes', 'Yes', '25/12/2015', '25/12/2015', 'MAVIS ADJEI', 240328154, 'SHS', 'WBM ZION SECONDARY SCHOOL', 'Parent', 'No', 'ROSES SCHOOL COMPLEX', 264241182, 17, 0),
(3, 'MAVIS', 'FORSON', 'Female', '2000-05-01', '-', 'No', 'No', '-', '-', 'EMELIA NORTEY', 0, 'SHS', '-', 'Guardian', 'Yes', '-', 551321297, 18, 0),
(4, 'STEPHEN', 'ADDEY', 'male', '2000-08-18', '-', 'Yes', 'Yes', '-', '-', 'JOSEPH NARTEY', 548611995, 'SHS', 'MAYAN KROBO SHS', 'Parent', 'Yes', 'NOPRAS AREA', 267611031, 17, 0),
(5, 'COMFORT', 'DEIWAA DANSO', 'Female', '2003-11-25', '-', 'No', 'No', '-', '-', 'MARGARET ANINAGYEI YEBOAH ', 0, 'JHS', 'MID TAFO PRESBY JHS', 'Guardian', 'Yes', 'ASIKAFO AMANTAM', 0, 14, 0),
(6, 'MOHAMMED ', 'SHAIBU ', 'male', '1999-10-01', 'rd 224', 'Yes', 'Yes', '25/12/2015', '25/12/2015', 'SOPHIA NARTEY', 272588611, 'SHS_GRADUATE', 'WBM ZION SHS', 'Guardian', 'No', 'MID TAFO NEW ROAD', 559947370, 18, 0),
(7, 'JESSICA', 'OYIE DARKO', 'Female', '2004-12-23', 'T00', 'Yes', 'No', '00/00/0000', '00/00/0000', 'SELINA OPPONGWAA TWUM', 243245449, 'JHS', 'CRIG JHS', 'Parent', 'Yes', 'AVENUE A,ASIKAFO AMANTAM', 0, 13, 0),
(8, 'DORA', 'OKYERE SEKYIWAA', 'Female', '2004-03-28', 'TH 13', 'Yes', 'Yes', '-', '16/04/2017', 'APPIAKORANG CECELIA', 547715776, 'JHS', 'CRIG JHS', 'Parent', 'Yes', 'NOPRAS AREA', 0, 14, 0),
(9, 'DANIEL ', 'AKRASI', 'male', '2002-03-12', 'te 277', 'Yes', 'No', '--', '--', 'EVELYN KORANTENG', 242150301, 'SHS', 'POPE JOHNS SHS', 'Parent', 'Yes', 'NANA HEMAA JUNCTION', 262395537, 16, 0),
(10, 'NINA', 'KORKOR WAYO', 'Female', '2001-05-08', 'TE 64', 'Yes', 'No', '-', '-', 'MRS MARGARET OBENG', 248295592, 'SHS_GRADUATE', 'WBM ZION SHS', 'Parent', 'Yes', 'ZONGO - OPPOSITE TAFO GOV&#039;T HOSPITAL', 560544251, 17, 0),
(11, 'JOYCE', 'TETTEH GYAAWA', 'Female', '2001-08-12', 'TE 205', 'No', 'No', '00/00/0000', '00/00/0000', 'MARY TETTEH AYIM', 541039181, 'SHS', 'WBM SENIOR HIGH SCHOOL,AMEZSS', 'Parent', 'Yes', 'MID TAFO,NEAR ROSES SCHOOL', 559948013, 17, 0),
(12, 'PRISCILLA', 'ASARE AGYEIWAA', 'Female', '2001-12-10', 'TH 25A', 'No', 'No', '00/00/0000', '00/00/0000', 'CHRISTIANA ASARE', 242219870, 'SHS_GRADUATE', 'SHS GRADUATE', 'Parent', 'Yes', 'MID TAFO NEW ROAD,NEAR OKO CAMP', 242219870, 17, 0),
(13, 'ROBERT', 'BOATENG', 'male', '2005-10-12', 'TE281', 'No', 'No', '-', '-', 'VERA BAAH', 0, 'JHS', 'ROSES SCHOOL COMPLEX', 'Parent', 'Yes', 'PENTECOST MID-TAFO', 0, 12, 0),
(14, 'JULIANA', 'OFORIWAA ANINAGYEI', 'Female', '2000-09-23', '-', 'Yes', 'No', '-', '-', 'MARGARET ANINAGYEI YEBOAH ', 244096865, 'SHS_GRADUATE', 'GHANA SENIOR HIGH KOFORIDUA', 'Parent', 'Yes', 'ASIKAFO AMANTAM', 0, 17, 0),
(15, 'AKOSUA', 'OFORI-DARKO', 'Female', '2003-08-03', 'th 34', 'Yes', 'No', '--', '--', 'MAGARET OFOSUA', 540774636, 'JHS', 'CRIG M/A JHS', 'Parent', 'Yes', 'NOPRAS FM', 0, 15, 0),
(16, 'ABRAHAM', 'ANOM', 'male', '2001-12-11', '-', 'No', 'No', '-', '-', 'YA W FRIMPONG', 241777098, 'JHS', 'MID TAFO JHS', 'Parent', 'No', 'AVENUE A', 0, 17, 0),
(17, 'LINDA', 'OFORI', 'Female', '2000-09-25', '-', 'Yes', 'No', '-', '-', 'GEORGE OFORI AKOI', 244436544, 'SHS', 'METHODIST GIRLS HIGH SCHOOL-MAMFE', 'Parent', 'Yes', 'COCOA VILLAGE - MID TAFO', 0, 17, 0),
(18, 'JENNIFER', 'ARMAH', 'Female', '2002-10-20', '00', 'Yes', 'No', '00/00/0000', '00/00/0000', 'ERICA SAKYIBEA', 243457041, 'SHS', 'ABUSCO,GENERAL ART', 'Parent', 'Yes', 'OHEMAA JUNCTION AREA', 552727387, 16, 0),
(19, 'ESTHER', 'ASARE', 'Female', '2003-01-24', '-', 'Yes', 'No', '--', '--', 'DORIS TETTEH', 242604228, 'JHS', 'CRIG M/A JHS', 'Parent', 'Yes', 'NANA HEMAA JUNCTION', 509408759, 15, 0),
(20, 'CHRISTIANA', 'AGYEKUMHENE BAIDEN', 'Female', '2002-01-14', 'TH 146', 'No', 'No', '00/00/0000', '00/00/0000', 'VIDA ANTWI', 242884738, 'SHS', 'KIBI SECTECH,GENERAL ARTS', 'Parent', 'No', 'BEHIND SERENE COTTAGE', 551687130, 16, 0),
(21, 'OSBORN', 'NARTEY OJERHTETTEY', 'male', '2004-10-29', 'NQ 3A', 'Yes', 'No', '-', '--', 'AUGUSTINE NARTEY', 208228151, 'JHS', 'CRIG JHS', 'Parent', 'Yes', 'TONTRO JUNCTION', 500015728, 14, 0),
(22, 'CHARLES', 'OWUSU ANIM', 'male', '2004-09-16', '-', 'Yes', 'No', '-', '-', 'MARY OSEI AGYEMANG', 243153051, 'JHS', 'GIFPRAISE JUNIOR HIGH SCHOOL', 'Parent', 'Yes', 'OHEMAA JUNCTION AREA - ANYANE', 0, 13, 0),
(23, 'ROSEMOND', 'ABUAA AMPOFO', 'Female', '2001-12-07', '-', 'Yes', 'No', '--', '--', 'FRED APPIAH AMPOFO', 201974803, 'SHS', 'NEW JUABEN SHS', 'Parent', 'Yes', 'NANA HEMAA JUNCTION', 203753666, 16, 0),
(24, 'LITECIA', 'DOGBE', 'Female', '2001-12-22', 'TF 114', 'No', 'No', '00/00/0000', '00/00/0000', 'MARY OTIWAA ASARE', 245863932, 'SHS', 'SAVIOR SENIOR HIGH,HOME ECONOMICS', 'Parent', 'Yes', 'TAFO ZONGO,BEHIND ZONGO PARK', 245863932, 17, 0),
(25, 'EMMANUEL', 'NTIAMOAH', 'male', '1999-05-01', '-', 'No', 'No', '-', '-', 'ANNIAGYEI YEBOAH', 0, 'JHS', 'MID TAFO JHS', 'Parent', 'No', 'ASIKAFO AMANTAM', 0, 18, 0),
(26, 'TIMOTHY', 'OKLETEY', 'male', '2004-06-06', '-', 'No', 'No', '-', '-', 'DEDE VIVIAN', 245691995, 'JHS', 'AMPONFI JHS', 'Parent', 'Yes', 'ASIKAFO AMANTAM', 503208278, 14, 0),
(27, 'DENNIS', 'AGYEMANG', 'male', '2003-03-24', 'NT225', 'No', 'No', '-', '-', 'OPHELIA MARFO', 0, 'primary', 'MID TAFO PRESBY', 'Parent', 'Yes', 'MT ZION PRESBY CHURCH', 0, 15, 0),
(28, 'PAUL', 'LARTEY', 'male', '2002-02-14', 'TF 100', 'Yes', 'No', '-', '26/12/2016', 'VICTORIA DANKYI', 243424918, 'SHS', 'WBM ZION SHS', 'Guardian', 'Yes', 'ASIKAFO AMANTAM', 559569057, 16, 0),
(29, 'EVELYN', 'NANA AMA POKUA BAAH ASANTE', 'Female', '2003-08-23', '-', 'Yes', 'No', '00/00/0000', '00/00/0000', 'BOAKYE YIADOM MERCY LARTEY', 240688438, 'SHS', 'METHODIST GIRLS', 'Guardian', 'No', 'PRESBY CHURCH -MID TAFO', 507570245, 15, 0),
(30, 'SHEILA', 'ADWOA SINDEITA', 'Female', '2003-01-06', 'E 25', 'No', 'No', '00/00/0000', '00/00/0000', 'GRACE BADUBA', 240246013, 'JHS', 'JULIET JOHNSON SCHOOL', 'Parent', 'Yes', 'NURSES QUARTERS -TAFO', 240246013, 15, 0),
(31, 'PRINCE', 'DUAH NUAMAH', 'male', '2001-05-17', 'TE 233', 'Yes', 'No', '-', '-', 'FLORENCE NUAMAH ', 244539636, 'SHS', 'SAVIOUR SHS - OSIEM', 'Parent', 'Yes', 'MID TAFO - BEHIND ROSES SCHOOL', 204025295, 17, 0),
(32, 'MAXMILLIAN', 'OSEI POKU', 'male', '2004-05-26', 'T19', 'Yes', 'No', '-', '-', 'OSEI KWAME POKU', 0, 'JHS', 'CRIG JHS', 'Parent', 'Yes', 'POLICE QUATERS', 550801592, 14, 0),
(33, 'PRISCILLA', 'OBENG AGYEI', 'Female', '2002-06-22', 'Te 281', 'No', 'No', '00/00/0000', '00/00/0000', 'BAAH WILLIAM', 244686777, 'JHS', 'CRIG M/A JHS', 'Guardian', 'Yes', 'AVENUE A', 261375483, 16, 0),
(34, 'NOAMI ', 'SARPOMAA', 'Female', '2001-01-01', 'SK/7', 'No', 'No', '00/00/0000', '00/00/0000', 'HANNAH BONZIQUAYE', 544774914, 'SHS', 'WINNEBA SCHOOL OF BUSINESS,HOME ECONOMICS', 'Parent', 'No', 'WINNEBA,SANKOR,VISITED', 547830602, 17, 0),
(35, 'NANA ASANTE', 'ANTWI-BOASIAKO', 'male', '2001-08-12', '-', 'No', 'No', '-', '-', 'JOYCE MINTAH DARKWA', 242010985, 'SHS', 'KOFORIDUA SECTECH', 'Parent', 'Yes', 'ASANA - BRIGHT SHS AREA', 0, 16, 0),
(36, 'SHADRACH', 'OTENG', 'male', '2003-03-22', 'TH23', 'No', 'No', '-', '-', 'EUNICE OTENG', 244940074, 'JHS', 'FEDEN M/A JHS', 'Parent', 'No', 'NEW ROAD', 0, 15, 0),
(37, 'ROLAND', 'AGYIN-BIRIKORANG', 'male', '2002-02-23', '-', 'No', 'No', '-', '-', 'REV. RM AMPONSAH', 243964194, 'JHS', 'CRIG JHS', 'Parent', 'Yes', 'MT ZION PRESBY CHURCH', 544797551, 16, 0),
(38, 'NELSON', 'GBOKPA', 'male', '1999-05-19', '-', 'No', 'No', '-', '-', 'REBECCA DZAMESI ', 576548366, 'SHS_GRADUATE', 'DELCAM SHS - ADENTA', 'Parent', 'Yes', 'NEW ROAD - OKO AREA', 551315920, 19, 0),
(39, 'PATIENCE', 'TEIKO', 'Female', '2006-01-07', '-', 'No', 'No', '00/00/0000', '00/00/0000', 'SALOMEY NUETEY', 0, 'primary', 'M/A ROSES SCHOOL', 'Guardian', 'Yes', 'NEW ROAD -MID TAFO', 0, 12, 0),
(40, 'LOVELACE', 'BAIDEN', 'male', '2005-02-20', 'TH146', 'No', 'Yes', '-', '-', 'VIDA ANTWI NTIAMOAH', 242884638, 'JHS', 'FEDEN M/A JHS', 'Parent', 'Yes', 'NDEW ROAD', 0, 13, 0),
(41, 'MATTHEW', 'TEYE', 'male', '2003-08-28', '-', 'No', 'No', '-', '-', 'TETTEH SAMUEL', 0, 'JHS', 'ROSES SCHOOL COMPLEX', 'Parent', 'No', 'BEHIND ROSES SCHOOL CONPLEX', 0, 15, 0),
(42, 'EMMANUEL', 'ASARE', 'male', '2002-04-14', 'TF 114', 'No', 'No', '00/00/0000', '00/00/0000', 'MARY OTIWAA ASARE', 245863932, 'JHS', 'MID TAFO PRESBY JHS', 'Parent', 'Yes', 'TAFO ZONGO,BEHIND ZONGO PARK', 551688963, 16, 0),
(43, 'OBED ', 'COFFIE', 'male', '2002-06-04', '-', 'Yes', 'Yes', '-', '-', 'SIMON COFFIE', 546650211, 'SHS', 'KIBI SEC TECH', 'Parent', 'Yes', 'KIDS PARADISE', 550393419, 15, 0),
(44, 'JULIANA', 'AWUKU', 'Female', '2000-02-06', 'TF 125', 'Yes', 'No', '-', '26/12/2016', 'GLADYS MAKU NARTEY', 248291174, 'SHS_GRADUATE', 'WBM ZION SHS', 'Parent', 'No', 'AVENUE A - MAMA VIC AREA', 240262917, 18, 0),
(45, 'VICTORIA', 'ADOMAA DAUTEY', 'Female', '2000-02-05', 'TF 184', 'No', 'No', '-', '-', 'FLORENCE ABENA DANSOA', 557702327, 'SHS_GRADUATE', 'WBM ZION SHS', 'Parent', 'Yes', 'ASIKAFO AMANTAM', 550392928, 18, 0),
(46, 'AGNES ', 'ANSAH DAUTEY', 'Female', '2002-09-29', 'TF 184', 'No', 'No', '-', '-', 'FLORENCE ABENA DANSOA', 557702327, 'JHS', 'MID TAFO PRESBY JHS', 'Parent', 'Yes', 'ASIKAFO AMANTAM', 0, 15, 0),
(47, 'MICHAELLA', 'SERWAA AGYEMANG', 'Female', '2006-03-03', '-', 'Yes', 'No', '-', '-', 'MRS THERESA OTENG', 0, 'primary', 'GIFPRAISE SCHOOL COMPLEX', 'Guardian', 'No', 'ASIKAFO AMANTAM', 0, 12, 0),
(48, 'FELIX ', 'OPOKU DARKO', 'male', '2003-08-08', '-', 'Yes', 'No', '-', '-', 'SELINA OPPONGWAA TWUM', 243245449, 'JHS', 'GIFPRAISE SCHOOL COMPLEX', 'Parent', 'Yes', 'ASIKAFO AMANTAM', 0, 15, 0),
(49, 'PRISCILLA', 'KORKOR TETTEH', 'Female', '2003-07-12', 'TE 205', 'No', 'No', '-', '-', 'DIANA TETTEH', 551835258, 'JHS', 'CRIG M/A JHS', 'Parent', 'Yes', 'MID TAFO - NEAR ROSES SCHOOL', 0, 15, 0),
(50, 'SAM', 'KWABLAH LOWOR', 'male', '2002-02-19', 'CRIG BUNGALOW 1', 'Yes', 'No', '00/00/000', '00/00/000', 'SAMUEL LOWOR', 204689880, 'SHS', 'PRESBYTERIAN BOYS SHS', 'Parent', 'Yes', 'CRIG BUNGALOW', 273862039, 16, 0),
(51, 'KWAME', 'OSAFO-APPIAH', 'male', '2001-10-06', '-', 'Yes', 'No', '-', '-', 'JOYCE OPOKU SOMUAH', 246394704, 'SHS', 'ST. PETER&#039;S SHS', 'Parent', 'Yes', 'NEW ROAD - BEHIND FITTING SHOP', 263251589, 16, 0);

-- --------------------------------------------------------

--
-- Table structure for table `marital`
--

CREATE TABLE `marital` (
  `id` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `marital`
--

INSERT INTO `marital` (`id`, `status`) VALUES
(1, 'single'),
(2, 'married'),
(3, 'divorced'),
(4, 'widowered'),
(5, 'widowed'),
(6, 'cohabitation');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `member_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bdate` date NOT NULL,
  `gender` varchar(175) COLLATE utf8_unicode_ci NOT NULL,
  `occupation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` int(10) NOT NULL,
  `house` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `church_leader` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `gen_group` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `intrest_group` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `communicant` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `confirmation_date` date NOT NULL,
  `baptism_date` date NOT NULL,
  `residence` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fam_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `workplace` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fam_phone` int(10) NOT NULL,
  `marital` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `membership` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `member_id`, `firstname`, `lastname`, `bdate`, `gender`, `occupation`, `telephone`, `house`, `address`, `church_leader`, `gen_group`, `intrest_group`, `communicant`, `confirmation_date`, `baptism_date`, `residence`, `fam_name`, `workplace`, `fam_phone`, `marital`, `membership`, `deleted`) VALUES
(1, 'MC', 'RICHARD', 'AMPONSAH MARFO', '1972-06-13', 'male', 'Salaried-Govt', 243964194, 'ATWIMA KOFORIDUA', 'MOUNTZION PRESBY MISSION HOUSE -MID TAFO', 'yes', 'Men Fellowship', '---', 'yes', '1000-01-01', '2001-04-07', 'ATWIMA KOFORIDUA', 'VIVIAN AMPONSAH MARFO', 'WBM ZION SHS - OLD TAFO', 545341229, 'married', 'Regular', 0),
(2, 'FC', 'DIVINE ', 'AFUA DEDE NARTEY', '1975-05-16', 'Female', 'Salaried-Govt', 202698559, 'NURSES QUARTERS 3A', 'NURSES QUARTERS ', 'yes', 'Women Fellowship', '---', 'yes', '1000-01-01', '1000-01-01', 'NURSES QUARTERS 3A', 'AUGUSTINE NARTEY', 'TAFO GOV&#039;T HOSPITAL', 208228151, 'married', 'Regular', 0),
(3, 'FC', 'JULIANA', 'AMAKYE', '1974-08-02', 'Female', 'Salaried-Govt', 242519887, 'TE 175A', 'KENAKROM', 'no', 'Women Fellowship', '---', 'yes', '1000-01-01', '1984-12-26', 'NURSES QUARTERS - MID TAFO', 'EMMANUEL SOMUAH', 'CHPS - KUKURANTUMI', 244248968, 'married', '', 0),
(4, 'MC', 'MORRISON', 'AMOAH OKYERE', '1985-12-03', 'male', 'Salaried-Govt', 246030590, 'TE 158A', 'OHEMAA JUNCTION AREA', 'yes', 'Young Adults Fellowship', '---', 'yes', '1000-01-01', '1000-01-01', 'OHEMAA JUNCTION AREA,MR. ASIBE&#039;S HOUSE', 'THERESA KWABEA', 'NEW TAFO ROSES M/A JHS', 246827743, 'married', '', 0),
(5, 'MC', 'KOFI', 'YEBOAH BOATENG', '1949-11-11', 'male', 'Non Salaried', 200651802, '------', 'BOAKYE&#039;S PARK', 'no', 'Men Fellowship', 'BSPG', 'yes', '1964-12-25', '1956-12-25', '------', 'KWABENA YEBOAH BOATENG', 'NOBI -NEW TAFO', 541788299, 'single', 'Regular', 0),
(6, 'FC', 'THERESA', 'KWABEA', '1988-06-29', 'Female', 'Salaried-Govt', 246827743, 'TE 158', 'OHEMAA JUNCTION AREA', 'no', 'Young Peoples Guild', '---', 'yes', '1000-01-01', '1000-01-01', 'OHEMAA JUNCTION AREA,MR. ASIBE&#039;S HOUSE', 'MORRISON AMOAH OKYERE', 'TAFO GOVERNMENT HOSPITAL', 246030590, 'married', 'Regular', 0),
(7, 'FC', 'EMELIA', 'NARTEY', '1981-02-21', 'Female', 'Salaried -Private', 548611995, 'TH 30 - MID TAFO', 'NOPRAS FM - MID TAFO', 'no', 'Young Adults Fellowship', 'Ushering', 'yes', '1997-01-01', '1983-07-03', 'TH 30 - MID TAFO', 'JOSEPH NARTEY ADDEY', 'PRESBY PREPARATORY SCHOOL - MID TAFO', 245892647, 'married', 'Regular', 0),
(8, 'FC', 'FELICIA ', 'OWUSU ASIEDU', '1978-08-08', 'Female', 'Salaried-Govt', 246185952, 'TF 185', 'TONTRO JUNCTION', 'no', 'Young Adults Fellowship', '---', 'yes', '1999-12-26', '1000-01-01', 'TF 185', 'DANIEL OWUSU ASIEDU', 'OSEIM', 244929872, 'married', '', 0),
(9, 'FNC', 'FAUSTINA', 'OHENEWAA', '1960-08-23', 'Female', 'Salaried -Private', 545098358, 'C 44', 'SDA CHURCH,OSIEM\r\nMAAME ATAA ADWOA FIE', 'no', '---', 'Singing Band', 'no', '1000-01-01', '1970-01-01', 'OSIEM', 'VIVIAN SEFA', 'OSIEM', 545098358, 'single', '', 0),
(10, 'FC', 'RITA', 'OPARE ADAMSON', '1979-04-10', 'Female', 'Non Salaried', 500376789, '-', 'WILLIE KINGS HOUSE', 'no', 'Young Adults Fellowship', '---', 'yes', '1000-01-01', '1000-01-01', 'NEW TAFO ZONGO', 'KWABENA TWUM BARIMA', 'HAIDRESSER - MID TAFO', 243283949, 'married', '', 0),
(11, 'MC', 'AUGUSTINE', 'NARTEY', '1974-02-08', 'male', 'Salaried-Govt', 208228151, 'NURSES QUARTERS', 'NURSES QUATERS', 'yes', 'Men Fellowship', '---', 'yes', '1995-02-09', '1992-12-04', 'NURSES QUARTERS', 'DIVINE AFUA DEDE NARTEY', 'OYOKO METHODIST SHS - OYOKO, KOFORIDUA', 202698559, 'married', 'Regular', 0),
(12, 'FC', 'CHARLOTTE', 'ATUOBI', '1979-01-04', 'Female', 'Salaried -Private', 275693038, 'TF 155', 'TAFO COURT', 'no', 'Young Adults Fellowship', '---', 'yes', '1000-01-01', '1000-01-01', 'TF 155', 'BENJAMIN ATUOBI', 'GIFPRAISE SCHOOOL', 579585567, 'married', 'Regular', 0),
(13, 'MC', 'LLOYD', 'TWUM-AMPOFO', '1984-06-05', 'male', 'Salaried-Govt', 246881023, 'NURSES QUARTERS - MID TAFO', 'NURSES QUARTERS - MID TAFO', 'yes', 'Young Adults Fellowship', '---', 'yes', '2002-12-26', '1000-01-01', 'NURSES QUARTERS - MID TAFO', 'NANCY OPOKUAA TWUM-AMPOFO ', 'RHEMA JHS - NEW TAFO', 249651411, 'married', 'Regular', 0),
(14, 'FNC', 'MAVIS', 'AWATEY', '1994-05-01', 'Female', 'Unemployed', 542302018, '000', 'BETHEL PRESBY AREA,', 'no', 'Young Peoples Guild', '---', 'no', '2012-12-23', '2012-12-26', 'MAASE', 'AGNES AWATEY', 'UNEMPLOYED', 542302080, 'single', '', 0),
(15, 'FC', 'NANCY', 'OPOKUAA TWUM-AMPOFO', '1987-05-29', 'Female', 'Non Salaried', 249651411, 'NURSES QUARTERS - MID TAFO', 'NURSES QUARTERS - MID TAFO', 'no', 'Young Adults Fellowship', 'Singing Band', 'yes', '2007-12-30', '2007-12-30', 'NURSES QUARTERS - MID TAFO', 'LLOYD TWUM-AMPOFO', 'TRADING', 246881023, 'married', 'Regular', 0),
(16, 'FC', 'EVELYN', 'KORANTENG OBISIW', '1960-08-24', 'Female', 'Salaried -Govt', 501592514, 'TE 277', 'NANA HEMAA JUNCTION', 'yes', 'Women Fellowship', '---', 'yes', '1000-01-01', '0001-01-01', 'TE 277', 'MOSES SARPONG', 'TAFO GOVERNMENT HOSPITAL', 241465537, 'married', 'Regular', 0),
(17, 'FC', 'WINIFRED', 'OFORIWAA KUMI', '1970-11-19', 'Female', 'Salaried-Govt', 200717786, 'KNUST,KUMASI', 'CRIG PRIMARY SCHOOL', 'yes', 'Women Fellowship', '---', 'yes', '1989-09-03', '1971-02-14', 'KNUST,KUMASI', 'F.C MILLS-ROBERTSON', 'CRIG', 208970091, 'married', 'Regular', 0),
(18, 'FC', 'OPHELIA', 'MARFO', '1985-07-04', 'Female', 'Salaried -Govt', 243753260, 'TF 225', 'MID TAFO PREPARATORY SCHOOL', 'no', 'Young Adults Fellowship', '---', 'yes', '1000-01-01', '1000-01-01', 'TF 225', 'ALEXANDER SACKEY AYERTEY', 'METHODIST JHS', 247701338, 'married', 'Regular', 0),
(19, 'FC', 'FLORENCE', 'YEBOAH NUAMAH', '1963-08-18', 'Female', 'Non Salaried', 244539636, 'KUMASI', 'BEHIND ROSES SCHOOL - MID TAFO', 'no', 'Women Fellowship', 'Ushering', 'yes', '1982-12-25', '1982-12-25', 'KUMASI', 'RAYMOND BANINI', 'TRADING', 244290133, 'married', 'Regular', 0),
(20, 'MNC', 'OFORI', 'BOA-AMPONSEM KWABENA', '1996-05-21', 'male', 'Student', 241056349, 'TF 119A', 'PENTECOST CHURCH OLD TAFO', 'no', 'Young Peoples Guild', '---', 'no', '1000-01-01', '1000-01-01', 'OLD TAFO', 'RICHARD OFORI BOA-AMPONSEM', 'COH,KINTAMPO', 242381616, 'single', '', 0),
(21, 'FC', 'FRANCISCA', 'AKWABOA', '1990-08-01', 'Female', 'Student', 501377384, '---', 'NANA HEMAA JUNCTION', 'no', 'Young Peoples Guild', '---', 'yes', '2017-12-26', '2017-12-26', 'C 18 NSAWAM', 'BENJAMIN KWESI KONADU', 'MINISTRY OF FOOD AND AGRIC', 277162847, 'single', '', 0),
(22, 'FC', 'GRACE', 'COFFIE', '1981-02-25', 'Female', 'Non Salaried', 240440031, 'MID TAFO ', 'BEHIND MR. AMPOFO&#039;S HOUSE - MID TAFO', 'no', 'Women Fellowship', 'Ushering', 'yes', '1995-09-03', '1995-09-03', 'MID TAFO ', 'SIMON COFFIE', 'TRADING', 546650211, 'married', '', 0),
(23, 'FC', 'COMFORT', 'ADOMAA AMOAKO', '1943-02-15', 'Female', 'Retired', 246450154, 'BUNGALOW NUMBER 11/CRIG', 'CRIG PRIMARY SCHOOL', 'no', 'Women Fellowship', '---', 'yes', '1000-01-01', '1000-01-01', 'CRIG BUNGALOW NUMBER 11', 'WINIFRED OFORIWAA KUMI', 'NATIONAL LOTTRIES', 244206333, 'married', '', 0),
(24, 'FC', 'LINA', 'KPAKPOE', '1984-07-09', 'Female', 'Salaried -Govt', 277448976, '-', 'NANA HEMAA JUNCTION', 'yes', 'Young Adults Fellowship', '---', 'yes', '2002-12-25', '1991-12-21', 'NUNGUA - ACCRA', 'ROBERT ALABI', 'GHANA COCOA BOARD', 208957883, 'married', '', 0),
(25, 'FC', 'CHRISTIANA', 'NYARKO', '1973-05-25', 'Female', 'Non Salaried', 242219870, 'TH 25A', 'NOPRAS FM', 'no', 'Women Fellowship', 'Church Choir', 'yes', '1000-01-01', '1000-01-01', 'TH 25A', 'KWAME ASARE', 'ZONGO FILLING STATION', 261986963, 'married', '', 0),
(26, 'FC', 'ERNESTINA', 'MANUKURE', '1992-09-16', 'Female', 'Salaried -Govt', 553435024, '000', 'OHEMAA JUNCTION AREA,PENTECOST CHURCH', 'no', 'Young Peoples Guild', 'Singing And Praises Team', 'yes', '1000-01-01', '1000-01-01', 'SUNYANI,DOMINASE', 'BENJAMIN MANUKURE', 'TAFO GOVERNMENT HOSPITAL', 243463468, 'single', '', 0),
(27, 'FC', 'JOYCE', 'AKOSUA SARPONG', '1970-11-22', 'Female', 'Salaried -Private', 540822968, 'TH 17', 'NOPRAS FM', 'no', 'Women Fellowship', '---', 'yes', '1000-01-01', '1000-01-01', 'TH 17', 'EDITH DANSOA', 'TONTRO JUNCTION', 542412942, 'married', '', 0),
(28, 'FC', 'VIVIAN', 'AMPONSAH MARFO', '1978-11-17', 'Female', 'Non Salaried', 545341229, '-', 'MID TAFO PRESBY CHURCH', 'no', 'Young Adults Fellowship', '---', 'yes', '1000-01-01', '1000-01-01', 'PRESBY MISSION HOUSE - MID TAFO', 'REV. RICHARD AMPONSAH MARFO', 'BAKER', 243964194, 'married', '', 0),
(29, 'FC', 'ELIZABETH', 'OSEI', '1954-05-29', 'Female', 'Retired', 242839891, 'OSINO', 'BLEUMICH HOTEL AREA', 'no', 'Women Fellowship', 'Church Choir', 'yes', '1968-08-11', '1955-06-12', 'OSINO', 'ASANTEWAA PATIENT', 'NEW TAFO PRESBYTERIAN KG', 541604006, 'divorced', '', 0),
(30, 'FC', 'ESTHER', 'OFORI NKANSAH', '1983-09-02', 'Female', 'Non Salaried', 243067780, 'TF 225', 'MID TAFO PRESBY CHUCH', 'no', 'Young Adults Fellowship', 'Singing Band', 'yes', '1000-01-01', '1000-01-01', 'MID TAFO NEW ROAD', 'WILLIAMS OFORI NKANSAH', 'TRADING', 243072852, 'married', '', 0),
(31, 'FC', 'JOYCE', 'MINTAH DARKWAH', '1968-02-28', 'Female', 'Salaried -Govt', 242010985, '-', 'BRIGHT SENIOR HIGH SCHOOL', 'no', 'Women Fellowship', 'BSPG', 'yes', '1000-01-01', '1000-01-01', 'KUKURANTUMI', 'NANA ANTWI BOASIAKO ASENSO', 'CHED', 208165957, 'married', '', 0),
(32, 'FC', 'MARY', 'OSEI AGYEMANG', '1978-08-09', 'Female', 'Salaried -Govt', 243153051, 'TE 333', '-', 'no', 'Young Adults Fellowship', '---', 'yes', '1000-01-01', '1000-01-01', 'TE 333 - NEW TAFO', 'ALBERT QUARTEY', 'RHEMA SCHOOL - NEW TAFO', 242004260, 'married', '', 0),
(33, 'FC', 'RUTH', 'OWUSUAA', '1984-11-03', 'Female', 'Salaried -Govt', 249528013, '-', 'ADONTENG SHS', 'no', 'Young Adults Fellowship', 'Singing And Praises Team', 'yes', '1000-01-01', '2005-05-19', 'KUKURANTUMI', 'WILLIAM OSEI OWUSU', 'R/C BASIC SCHOOL - NEW TAFO', 243734380, 'married', '', 0),
(34, 'FC', 'ROSE', 'LOWOR', '1961-02-28', 'Female', 'Salaried -Govt', 243889878, 'CRIG BUNGALO 1', 'CRIG BUNGALOW 1', 'yes', 'Women Fellowship', 'Church Choir', 'yes', '1977-06-12', '1972-11-12', 'CRIG BUNGALO 1', 'DR SAMUEL T. LOWOR', 'CRIG TAFO', 243889880, 'married', 'Regular', 0),
(35, 'FC', 'SALOMEY', 'MAMLEY NORTEY', '1952-09-15', 'Female', 'Salaried -Govt', 246911200, '-', 'NEW ROAD', 'no', 'Women Fellowship', '---', 'yes', '1970-02-15', '1953-06-14', 'NEW ROAD ', 'E.N. NORTEY', 'CRIG - NEW TAFO', 246383735, 'married', '', 0),
(36, 'MC', 'SAMUEL', 'T. LOWOR', '1963-09-16', 'male', 'Salaried -Govt', 204689880, 'CRIG BUNGALOW', 'CRIG BUNGALOW', 'no', 'Men Fellowship', '---', 'yes', '1980-03-02', '1964-08-09', 'CRIG BUNGALOW', 'ROSE LOWOR', 'CRIG - NEW TAFO', 243889878, 'married', 'Regular', 0),
(37, 'MC', 'WILLIAM', 'OSEI OWUSU', '1985-03-19', 'male', 'Salaried -Govt', 243734380, '-', 'BEHIND ADONTENG SHS', 'yes', 'Young Adults Fellowship', '---', 'yes', '2000-12-26', '0001-01-01', 'KUKURANTUMI', 'RUTH OWUSUA', 'WBM ZION SHS OLD TAFO', 249528013, 'married', '', 0),
(38, 'MC', 'FRED', 'APPIAH-AMPOFO', '1961-12-25', 'male', 'Salaried -Govt', 201974803, 'MID TAFO', 'OHEMAA JUNCTION AREA', 'yes', 'Men Fellowship', '---', 'yes', '1962-05-14', '1962-04-22', 'MID TAFO', 'JOYCE APPIAH-AMPOFO', 'CRIG PRIMARY', 242358441, 'married', 'Regular', 0),
(39, 'FC', 'JOYCE', 'APPIAH-AMPOFO', '1964-12-01', 'Female', 'Non Salaried', 242358441, 'MID TAFO', 'OHEMAA JUNCTION AREA', 'no', 'Women Fellowship', 'Church Choir', 'yes', '1981-12-27', '1972-12-24', 'MID TAFO', 'FRED APPIAH-AMPOFO', 'SEAMSTRESS - MID TAFO', 201974803, 'married', 'Regular', 0),
(40, 'MC', 'EDMUND', 'TWUM-AMPOFO', '1997-02-13', 'male', 'Student', 203580778, 'MID TAFO', 'OHEMAA JUNCTION AREA ', 'no', 'Young Peoples Guild', '---', 'yes', '2013-12-01', '2002-12-26', 'MID TAFO', 'FRED APPIAH-AMPOFO', 'KNUST', 201974803, 'single', 'Regular', 0),
(41, 'MC', 'SAMUEL', 'AWUAH SARPONG', '1997-07-18', 'male', 'Student', 501592514, 'TE 277', 'NANA HEMAA JUNCTION', 'no', 'Young Peoples Guild', '---', 'yes', '2015-12-15', '2015-12-15', 'TE 277', 'EVELYN KORANTENG', 'KNUST', 242150301, 'single', '', 0),
(43, 'FC', 'ESTHER', 'OBENG KORANTENG', '1977-11-28', 'Female', 'Salaried -Govt', 243005637, '000', 'OHEMAA JUNCTION AREA,NEAR ANTE HONOR&#039;S HOUSE', 'no', 'Women Fellowship', '---', 'yes', '1000-01-01', '1000-01-01', 'OBUASI', 'OENG FRANCIS', 'OLD TAFO ROMAN SCHOOL,PRIMARY', 244185807, 'married', '', 0),
(44, 'FNC', 'VICTORIA ', 'OFORIWAA BOATENG', '1984-10-24', 'Female', 'Non Salaried', 246041362, 'TF 146A', 'MID TAFO', 'no', 'Young Adults Fellowship', '---', 'no', '1000-01-01', '1000-01-01', 'TF 146A', 'FAUSTINA OFORI SAN AYEKOR', 'MID TAFO', 547900253, 'cohabitation', '', 0),
(45, 'FC', 'COMFORT', 'BOATEMAA SARPONG', '1994-06-24', 'Female', 'Student', 243873805, 'TAFO', 'OHEMAA JUNCTION AREA,NEAR MR. DAMPARE&#039;S HOUSE', 'no', 'Young Peoples Guild', '---', 'yes', '1000-01-01', '1000-01-01', 'TAFO', 'EVELYN KORANTENG', 'LEGON', 242150301, 'single', 'Regular', 0),
(46, 'FC', 'MARY', 'AMPONSAH', '1957-10-20', 'Female', 'Retired', 200450079, 'TC 42 A', 'ADABRAKA,TAFO.BEHIND THE PUBLIC TOILET', 'no', 'Women Fellowship', '---', 'yes', '1000-01-01', '1000-01-01', 'TAFO', 'GLORIA ANOMAH ADU', 'CRIG TAFO', 2147483647, 'widowed', '', 0),
(47, 'MC', 'JOHN', 'ARJARQUAH JNR', '1996-08-15', 'male', 'Student', 545691399, 'MID TAFO', 'ADJACENT GIFPRAISE JHS MID -TAFO', 'no', 'Young Peoples Guild', 'Prayer', 'yes', '2015-12-25', '1000-01-01', 'MID TAFO', 'JOHN ARJARQUAH SNR', 'PRESBYTERIAN UNIVERSITY COLLEGE-GHANA', 244696016, 'single', 'Regular', 0),
(48, 'FC', 'VERONICA', 'MENSAH', '1956-05-12', 'Female', 'Non Salaried', 206251419, 'TD 56', 'CRIG JUNCTION ROAD,OPPOSITE HOSTEL', 'no', 'Women Fellowship', '---', 'yes', '1000-01-01', '1000-01-01', 'TAFO ZONGO', 'MARY', 'MARKET', 206251419, 'widowed', '', 0),
(49, 'FNC', 'VIVIAN', 'DEDE DJAMAKIH', '1997-05-17', 'Female', 'Non Salaried', 245691995, 'DE 42', 'ASIKAFO AMATAM', 'no', 'Women Fellowship', '---', 'no', '1982-04-11', '1982-04-11', 'DE 42', 'JOSEPH OKRATEY', 'OSIEM', 242043706, 'married', '', 0),
(50, 'FC', 'YAA', 'BONSU SOMUAH', '1997-07-24', 'Female', 'Student', 245959429, 'KINAKROM,TAFO', 'KINAKROM OPPOSITE NURSES QUARTERS', 'no', 'Young Peoples Guild', '---', 'yes', '2015-12-25', '2007-12-25', 'KINAKROM,TAFO', 'JULIANA AMAKYE', 'LEGON,ACCRA', 242519887, 'single', 'Regular', 0),
(51, 'MC', 'BENJAMIN', 'BOATEY OSAFO', '1973-10-01', 'male', 'Salaried -Govt', 246814849, 'TH 24D', ' NEW TAFO,NEW ROAD MID-TAFO PRESBY JUNCTION ', 'no', 'Men Fellowship', '---', 'yes', '1000-01-01', '1000-01-01', 'TAFO,NEW ROAD', 'THERESA OSEI-DEI', 'WBM ZION SHS, OLD TAFO', 248160430, 'married', '', 0),
(52, 'FC', 'FAUSTINA', 'OFORISAH ', '1947-04-11', 'Female', 'Unemployed', 547300253, 'TF 146A', 'MID TAFO', 'no', 'Women Fellowship', '---', 'yes', '1967-02-02', '1950-12-17', 'NEW TAFO', 'VICTORIA BOATENG', 'N/A', 0, 'widowed', '', 0),
(53, 'FC', 'DORIS', 'TEIKO TETTEH', '1959-09-10', 'Female', 'Salaried -Govt', 242604228, 'TE 275 B', 'OHEMAA JUNCTION AREA,MR.DAMPARE&#039;HOUSE', 'no', 'Women Fellowship', 'BSPG', 'yes', '1977-03-06', '1977-03-06', 'ACCRA', 'SYLVESTER ASARE', 'BOSOSO ROMAN PRIMARY SCHOOL', 242965616, 'married', '', 0),
(54, 'MC', 'EMMANUEL', 'AMON-PABI', '1981-07-19', 'male', 'Salaried -Govt', 277244718, 'MID TAFO', 'GIFPRAISE SCHOOL', 'no', 'Young Adults Fellowship', 'Church Choir', 'yes', '2000-10-01', '1882-07-02', 'MID TAFO', 'SYLVIA AMON-PABI', 'GHANA SCHOOL OF LAW', 273745534, 'married', '', 0),
(55, 'FC', 'HENEWAA ', 'BENNEH SARPONG-DUKER', '1979-06-14', 'Female', 'Salaried -Private', 205803503, 'TH/5', 'MID TAFO  NOPRAS FM', 'no', 'Women Fellowship', '---', 'yes', '2017-12-26', '2017-12-26', 'MID TAFO', 'ERIC SARPONG-DUKER', 'NOPRAS FM', 246578650, 'married', '', 0),
(56, 'FC', 'SALOMEY', 'OSEI OWUSU', '1957-09-10', 'Female', 'Retired', 547455805, 'TF 239', 'OHEMAA JUNCTION AREA,NEAR MRS ARJARQUAH&#039;S HOUSE', 'no', 'Women Fellowship', '---', 'yes', '1000-01-01', '1000-01-01', 'KUMASI', 'DOREEN OSEI OWUSU', 'BOSOSO ISLAMIC JHS', 242605614, 'single', '', 0),
(57, 'MC', 'EBENEZER', 'ADOMAKO-MENSAH', '1994-07-03', 'male', 'Student', 267670987, 'TF 53A', 'ASSEMBLIES OF GOD CHURCH, NEW TAFO-AKIM', 'no', 'Young Peoples Guild', 'Ushering', 'yes', '0001-01-01', '0001-01-01', 'TF 53A', 'MRS BEATRICE OFORIWAA CHARTEY', 'UNIVERSITY OF MINES AND TECHNOLOGY, UMaT, TARKWA', 208097243, 'single', 'Regular', 0),
(58, 'FC', 'CECILIA', 'APPIAH-KORANG', '1983-10-25', 'Female', 'Non Salaried', 547715776, 'TH 13', 'NOPRAS FM.', 'no', 'Young Adults Fellowship', 'Singing Band', 'yes', '2000-12-25', '2000-12-25', 'DISTANT,ABOMOSU AND TAFO', 'MARY DOKUA', 'TRADER,MID TAFO', 246515504, 'divorced', '', 0),
(59, 'MC', 'PATRICK', 'LARTEY DJABA', '1979-09-09', 'male', 'Salaried -Govt', 242708857, 'TE 141', 'NEW TAFO ZONGO, COUNCIL', 'no', 'Young Adults Fellowship', 'BSPG', 'yes', '2000-12-25', '2000-12-25', 'NEW TAFO', 'VICTORIA DJABA', 'BOSUSU SDA JHS', 245106417, 'married', '', 0),
(60, 'FC', 'VICTORIA ', 'TEIKO DJABA', '1986-07-27', 'Female', 'Salaried -Private', 245106417, 'TE 141', 'NEW TAFO ZONGO, COUNCIL', 'no', 'Young Adults Fellowship', 'BSPG', 'yes', '1000-01-01', '2006-04-19', 'TE 141', 'PATRICK LARTEY DJABA', 'NEW TAFO-AKIM', 242708857, 'married', '', 0),
(61, 'MC', 'ANTHONY', 'TETTEH AYERNOR', '1960-09-28', 'male', 'Non Salaried', 555778040, 'KB 64', 'PRESBY CHURCH, KUKURANTUMI', 'no', 'Men Fellowship', 'Singing Band', 'yes', '1000-01-01', '1963-06-04', 'KURANTUMI', 'ANTHONY', 'KURANTUMI', 555778040, 'single', '', 0),
(62, 'FC', 'RITA', 'ADOMAKO-MENSAH', '1997-10-12', 'Female', 'Student', 560185050, 'TF 53A', 'AVENUE A', 'no', 'Young Peoples Guild', 'Singing And Praises Team', 'yes', '2016-03-25', '2010-12-26', 'HOSPITAL JUNCTION', 'BEATRICE OFORIWAA', 'COLLEGE OF HEALTH,KINTAMPO', 248954281, 'single', 'Regular', 0),
(63, 'FC', 'CONSTANCE', 'AFUA OBENEWAA', '1949-01-01', 'Female', 'Non Salaried', 261612214, 'MID TAFO', 'NEW TAFO', 'no', 'Women Fellowship', '---', 'yes', '1000-01-01', '1999-08-01', 'NEW TAFO', 'OSAFO BOATENG', '--', 20, 'divorced', '', 0),
(64, 'MC', 'CHARLES', 'ADEIKO', '1987-06-13', 'male', 'Salaried -Govt', 249679482, 'TE 270 B', 'NEW TAFO', 'no', 'Young Adults Fellowship', '---', 'yes', '2004-02-01', '1111-01-01', 'NEW TAFO ', 'AUGUSTINA OFOSU', 'NEW TAFO GOVERNMENT HOSPITAL', 241410120, 'married', '', 0),
(65, 'FC', 'THEODOSIA', 'YEBOAH', '1989-06-20', 'Female', 'Salaried -Govt', 248312578, 'TC86', 'MID-TAFO', 'no', 'Young Peoples Guild', 'BSPG', 'yes', '0001-01-01', '0001-01-01', 'MID-TAFO', 'ELIZABETH AKYAA', 'MOUNT ZION PRESBY', 244061122, 'married', '', 0),
(66, 'FNC', 'ABENA', 'SERWAA', '1965-02-26', 'Female', 'Non Salaried', 243581138, 'TF101', 'CRIG QUATERS', 'no', 'Women Fellowship', 'Singing Band', 'no', '0001-01-01', '0001-01-01', 'KUMASI', 'EMMANUEL KOFFIE  ', 'CRIG', 244289805, 'single', '', 0),
(67, 'MNC', 'ALEX', 'KWAME ABREKWAMA', '1990-12-01', 'male', 'Non Salaried', 247773128, 'TF ', 'MID TAFO, ROSES PREPARATORY SCHOOL', 'no', 'Young Peoples Guild', '---', 'yes', '1000-01-01', '1000-01-01', 'NEW TAFO', 'MARY', 'NEW TAFO-AKIM', 248713407, 'single', '', 0),
(68, 'MC', 'BENJAMIN', 'ATUOBI', '1071-03-03', 'male', 'Non Salaried', 579585567, 'TF 115', 'TAFO COURT', 'no', 'Men Fellowship', '---', 'yes', '1000-01-01', '1990-12-31', 'TF 115', 'CHARLOTTE ATUOBI', 'MID TAFO', 275693038, 'married', '', 0),
(69, 'FNC', 'AMA', 'SERWAA', '1965-02-26', 'Female', 'Unemployed', 243581138, 'TF101', 'CRIG QUATERS', 'no', 'Women Fellowship', 'Singing Band', 'no', '0001-01-01', '0001-01-01', 'CRIG QUATERS', 'EMMANUEL KOFFIE', 'CRIG QUATERS', 244289805, 'divorced', '', 0),
(70, 'FC', 'ROSEMOND', 'SAKYI', '1981-08-24', 'Female', 'Salaried -Govt', 243674873, 'TONTRO', 'TONTRO, CHPS COMPOUND', 'no', 'Young Adults Fellowship', 'Singing And Praises Team', 'yes', '1111-01-01', '1000-01-01', 'AKYEM SEKYERE', 'COMFORT AKYEAMAH', 'TONTRO CHPS COMPOUND', 243674873, 'single', '', 0),
(71, 'MC', 'JUSTICE', 'AGYAPONG', '1982-03-13', 'male', 'Non Salaried', 240407903, 'KONONGO', 'NANA HEMAA', 'no', 'Young Peoples Guild', 'Church Choir', 'yes', '2017-12-26', '1999-12-03', 'KONONGO', 'JULIANA AKUMI', 'MID TAFO', 243852288, 'single', '', 0),
(72, 'MNC', 'ENOCK', 'BROMAH', '1977-05-12', 'male', 'Non Salaried', 542599560, 'TE83', 'MID-TAFO', 'no', 'Young Adults Fellowship', '---', 'no', '0001-01-01', '0001-01-01', 'NEW TAFO ZONGO', 'BAABA', 'NEW TAFO', 249152285, 'widowered', '', 0),
(73, 'MC', 'PATRICK', 'SAPPOR', '1989-05-21', 'male', 'Salaried -Govt', 249037434, 'A 102', 'OLD TAFO SDA CHURCH', 'no', 'Young Peoples Guild', '---', 'yes', '2013-12-29', '2013-12-29', 'OLD TAFO', 'ADWOA DANSOWAA', 'OLD TAFO, ZION PRIMARY SCHOOL', 202724318, 'single', '', 0),
(74, 'FC', 'JANET', 'AMOAKOAH FRIMPONG', '1960-10-20', 'Female', 'Non Salaried', 203785596, 'MID TAFO NEW ROAD', 'NEW ROAD NEAR THE FITTING SHOP', 'no', 'Women Fellowship', 'Singing Band', 'yes', '0001-01-01', '0001-01-01', 'MID TAFO NEW ROAD', 'ELIZABETH POKUA FRIMPONG', 'MID TAFO', 203785598, 'married', '', 0),
(75, 'MNC', 'FOSTER', 'ADJEI KUMI', '1989-07-24', 'male', 'Salaried -Govt', 551684024, 'TF 141', 'NEW ROAD NEW TAFO', 'no', 'Young Peoples Guild', '---', 'yes', '1000-01-01', '1000-01-01', 'NEW TAFO', 'NAOMI ADJEI', 'MOUNTZION  NEW TAFO', 241915874, 'single', '', 0),
(76, 'FC', 'ELIZABETH', 'ABETEY', '1992-05-28', 'Female', 'Salaried -Private', 545316689, 'TF 122', 'MOSQUE ZONGO', 'no', 'Young Peoples Guild', 'Church Choir', 'yes', '1000-01-01', '1000-01-01', 'NEW TAFO', 'ABETEY TIMOTHY', 'SATHYA SAI SCHOOL', 240122220, 'single', '', 0),
(77, 'FC', 'MARY', 'KEMAWUSOR DEDE', '1986-04-05', 'Female', 'Salaried -Private', 541369844, '-', 'MID TAFO', 'no', 'Young Adults Fellowship', 'Singing Band', 'yes', '2004-12-26', '2003-12-26', 'MID TAFO', 'TEKPERTEY PETER', 'NEW-TAFO GOVT. HOSPITAL', 244257174, 'single', '', 0),
(78, 'FC', 'ELIZABETH', 'POKUAA FRIMPONG', '1988-03-18', 'Female', 'Student', 203785598, 'MID TAFO NEW ROAD', 'MID TAFO NEW ROAD NEAR THE FITTING SHOP', 'no', 'Young Peoples Guild', 'Singing Band', 'yes', '2007-04-08', '1997-03-30', 'MID TAFO NEW ROAD', 'GLADYS OFORIWAA FRIMPONG', 'WESTERN HILLS SCHOOL OF NURSING', 247154603, 'single', '', 0),
(79, 'FNC', 'BELINDA', 'OFORI', '1988-06-12', 'Female', 'Non Salaried', 240539322, 'A 32', 'MID TAFO, MT. ZION PRESBY CHURCH', 'no', 'Young Peoples Guild', '---', 'no', '1000-01-01', '1000-01-01', 'OLD TAFO', 'FLORENCE AGYAKOMAH', 'MARKET, TAFO', 277194057, 'single', '', 0),
(80, 'FC', 'SALOME', 'OFFEIBEA DARKO', '1982-07-25', 'Female', 'Salaried -Govt', 243125186, 'ET 257', 'FORMER GIFPRAISE JHS', 'no', 'Young Adults Fellowship', '---', 'yes', '2005-08-02', '1982-08-08', 'ET 257', 'OPPONG AMOASI SAMUEL', 'SAVIOUR SHS', 242325218, 'married', '', 0),
(81, 'FC', 'ALICE', 'APERKO', '1989-03-16', 'Female', 'Salaried -Private', 541882906, 'TE171', 'ROSES ', 'yes', 'Young Peoples Guild', 'BSPG', 'yes', '0001-01-01', '0001-01-01', 'NANA HEMAA JUNCTION', 'FRED AMPOFO', 'NEW TAFO', 201974803, 'married', '', 0),
(82, 'FC', 'GLADYS', 'OFORIWAA FRIMPONG', '1992-10-27', 'Female', 'Salaried -Govt', 247154603, 'TH23E', 'ANKAASE GOVERNMENT HOSPITAL ', 'no', 'Young Peoples Guild', '---', 'yes', '2007-04-08', '1997-03-30', 'MID TAFO NEW ROAD', 'ELIZABETH POKUA FRIMPONG', 'ANKAASE GOVERNMENT HOSPITAL', 203785598, 'single', '', 0),
(83, 'FC', 'BERNICE', 'NARKI BOATENG', '1989-04-22', 'Female', 'Salaried -Govt', 246310116, 'A 30', 'MID TAFO, NANA HEMAA JUNCTION', 'no', 'Young Adults Fellowship', 'Ushering', 'yes', '1000-01-01', '1000-01-01', 'MID TAFO', 'BRIGHT APPIAH BRAKO', 'NEW TAFO ROMAN CATHOLIC', 246310116, 'married', '', 0),
(84, 'FC', 'DOROTHY', 'OSAFO KWAKYE', '1963-12-20', 'Female', 'Non Salaried', 246826701, 'TH23D', 'NEW ROAD', 'yes', 'Women Fellowship', 'Singing Band', 'yes', '0001-10-10', '0001-01-01', 'NEW ROAD', 'SETH DUKU YEBOAH', 'NEW ROAD', 243317118, 'married', '', 0),
(85, 'FC', 'MABEL', 'FRIMPOMAA ', '1989-08-18', 'Female', 'Salaried -Private', 200161416, 'TH23E', 'MID TAFO NEW ROAD BEHIND THE FITTING SHOP', 'no', 'Young Peoples Guild', '---', 'yes', '2007-12-25', '1997-03-30', 'MID TAFO NEW ROAD ', 'ELIZABETH POKUA FRIMPONG', 'MID TAFO PRESBY PREPARATARY ', 203785598, 'single', '', 0),
(86, 'FC', 'REGINA', 'OBENEWAA', '1994-08-13', 'Female', 'Salaried -Govt', 543246658, '1', 'NSAWAM', 'no', 'Young Peoples Guild', 'Singing And Praises Team', 'yes', '2011-12-25', '2011-12-25', 'KWAKYE KROM NEAR NSAWAM', 'ELIZABETH POKUA FRIMPONG', 'ANAMIAPA PRIMARY SCHOOL', 203785598, 'single', '', 0),
(87, 'FC', 'MERCY', 'KORANTEMAA', '0001-01-01', 'Female', 'Unemployed', 0, '-', 'ROSES', 'no', 'Women Fellowship', '---', 'yes', '0001-01-01', '0001-01-01', '-', '-', '-', 0, 'single', '', 0),
(88, 'FC', 'MARY', 'OTIWAA', '1973-05-08', 'Female', 'Non Salaried', 245863932, 'ZONGO', 'TAFO COUNCIL', 'no', 'Women Fellowship', 'Singing Band', 'yes', '0100-01-01', '0100-01-01', 'ZONGO', 'LETICIA DOGBE', 'MID TAFO', 552428007, 'single', 'Regular', 0),
(89, 'FC', 'BEATRICE', 'KORAMAH AMPOFO', '1998-07-12', 'Female', 'Non Salaried', 554346450, '1', 'BLEUMERAGE HOTEL AREA', 'no', 'Young Peoples Guild', '---', 'no', '0001-01-01', '0001-01-01', 'KUKURANTUMI', 'DORCAS AMPOFOWAA', 'ECMAK JUNCTION AREA', 549213472, 'single', '', 0),
(90, 'FC', 'GLADYS', 'AYISI SEGUE', '1975-11-15', 'Female', 'Non Salaried', 247749303, 'TH 14B', 'NEW ROAD NEW TAFO', 'no', 'Potential WF', '---', 'yes', '1000-01-01', '1000-01-01', 'TH 14B', 'RICHARD SEGUOE', 'NKURAKAI', 240337411, 'single', 'Distant', 0),
(91, 'FC', 'ANGELA', 'OWUSU', '1986-08-08', 'Female', 'Salaried -Govt', 242057006, 'BEHIND ROSES SCHOOL COMPLEX,TAFO', 'DAHOMEY JUNCTION BEHIND ROSES SCHOOL', 'no', 'Young Adults Fellowship', 'Singing And Praises Team', 'yes', '2007-12-25', '2007-12-25', 'BEHIND ROSES SCHOOL COMPLEX,TAFO', 'ERIC KWAADWO ANIAKWA', 'MID TAFO PRESBY PREP. SCHOOL', 241391362, 'single', 'Regular', 0),
(92, 'MC', 'RICHARD', 'SEGUE', '1973-08-06', 'male', 'Salaried -Govt', 240337411, 'NEW TAFO,TH 14 B', 'NEW ROAD MID TAFO,OPPOSITE TREE GLOBAL COMPANY', 'yes', 'Men Fellowship', 'BSPG', 'yes', '1000-01-01', '1000-01-01', 'NEW TAFO,TH 14 B', 'AYISI GLADYS', 'CRIG TAFO', 247749303, 'married', 'Regular', 0),
(93, 'FC', 'DORIS', 'OSEI ASIBE', '1954-06-19', 'Female', 'Retired', 206603755, 'TE 158', 'KWAEBIBIRIM', 'yes', 'Women Fellowship', '---', 'yes', '1968-08-17', '1000-01-01', 'TE 158', 'OSEI ASIBE', 'NEW TAFO M/A JHS', 244781889, 'married', 'Regular', 0),
(94, 'FC', 'ANGELA', 'OSEI-POKU', '1987-03-12', 'Female', 'Unemployed', 506537901, 'T 19', 'POLICE QUARTERS - MID TAFO', 'no', 'Young Adults Fellowship', 'BSPG', 'yes', '0001-01-01', '0001-01-01', 'MID TAFO', 'KWAME OSEI-POKU', '-', 209212082, 'married', 'Regular', 0),
(95, 'FC', 'MARY', 'KORANTEMAH', '1931-01-01', 'Female', 'Unemployed', 0, '-', '-', 'no', 'Young Peoples Guild', '---', 'yes', '0001-01-01', '1996-11-24', '-', '-', '-', 0, 'single', 'Regular', 0),
(96, 'MC', 'GEORGE', 'OFORI AKOI', '1975-04-04', 'male', 'Salaried -Govt', 244436544, 'TE 296D', 'BEHIND CRIG QUATERS', 'yes', 'Men Fellowship', '---', 'yes', '1999-08-26', '1978-06-25', 'TE 296D', 'MARFOA SHALLEY', 'SAVIOUR SENIOR HIGH', 2147483647, 'married', 'Regular', 0),
(97, 'FC', 'SHALLEY ', 'MARFOA', '1980-06-12', 'Female', 'Salaried -Govt', 249246733, ' TE 295D', 'BEHIND CRIG QUATERS', 'no', 'Young Adults Fellowship', 'Church Choir', 'yes', '1995-12-25', '1995-12-25', 'TE 296D', 'GEORGE OFORI AKOI', 'KUKURANTUMI M/A PRIMARY', 244436544, 'married', 'Regular', 0),
(98, 'MC', 'EBENEZER', 'KWESI DJANGBA', '1985-01-13', 'male', 'Salaried -Govt', 242201438, '---', 'BEHIND NEW ROAD', 'no', 'Young Adults Fellowship', 'Ushering', 'yes', '0001-01-01', '1999-06-06', '--', 'TETTEH NARTEY REGINA', 'ADAWSO PRESBY JHS', 246733466, 'married', 'Regular', 0),
(99, 'FC', 'REGINA', 'TETTEH NARTEY', '1988-05-25', 'Female', 'Salaried -Govt', 246733466, '---', 'BEHIND NEW ROAD', 'no', 'Potential YAF', '---', 'yes', '1000-01-01', '1000-01-01', '--', 'EBENEZER DJAMGBA', 'NEW TAFO GOVT HOSPITAL', 242201438, 'married', 'Regular', 0),
(100, 'FC', 'SALOMEY', 'ANKOMAH', '1991-09-21', 'Female', 'Salaried -Govt', 543779350, 'D9/1', 'CRIG QUARTERS', 'no', 'Young Peoples Guild', '---', 'yes', '2011-01-01', '1991-12-26', 'D9/1', 'LYOLD NYAKPO', 'NEW TAFO GOVT HOSPITAL', 244972568, 'single', 'Regular', 0),
(101, 'FNC', 'REGINA', 'AYIMAA', '1992-09-17', 'Female', 'Unemployed', 273466662, 'TH 23B', 'NEW TAFO NEW ROAD', 'no', 'Young Peoples Guild', '---', 'no', '2017-04-16', '2017-04-16', 'TH 23B', 'KINGSLEY ASAMOAH KRODUA', '----', 277862214, 'single', 'Regular', 0),
(102, 'FC', 'BEATRICE', 'OFORIWAA CHARTEY', '1965-08-07', 'Female', 'Non Salaried', 248954281, 'TF53A', 'NEW TAFO', 'no', 'Women Fellowship', 'Church Choir', 'yes', '0001-01-01', '0001-01-01', 'NEW TAFO', 'RITA ADOMAKO-MENSAH', 'HOSPITAL JUNCTION', 555767797, 'married', 'Regular', 0),
(103, 'FC', 'MAVIS', 'ADOMAKO-MENSAH', '1992-08-16', 'Female', 'Unemployed', 547431998, 'TF53A', 'NEW TAFO', 'no', 'Young Peoples Guild', 'Singing And Praises Team', 'yes', '0001-01-01', '0001-01-01', 'NEW TAFO', 'BEATRICE OFORIWAA CHARTEY', 'kumasi', 248954281, 'single', 'Distant', 0),
(104, 'MC', 'JOSHUA', 'CHARTEY', '1961-10-19', 'male', 'Salaried -Govt', 549157611, 'TF53A', 'NEAR ASSEMBLIES OF GOD CHURCH', 'no', 'Men Fellowship', '---', 'yes', '0001-01-01', '0001-01-01', 'AVENUE A', 'BEATRICE OFORIWAA CHARTEY', 'BEGORO', 248954281, 'married', 'Regular', 0),
(105, 'FNC', 'VERA', 'BAAH', '1980-01-31', 'Female', 'Salaried -Govt', 243756224, 'TE281', 'MID TAFO', 'no', 'Young Adults Fellowship', '---', 'no', '0001-01-01', '0001-01-01', 'MID TAFO', 'W.K.BAAH', 'ANYINASIN', 244686777, 'single', 'Regular', 0),
(106, 'FC', 'VICTORIA', 'AJARQUAH', '1954-04-20', 'Female', 'Salaried -Govt', 245970115, 'TE 268', 'MID TAFO', 'no', 'Women Fellowship', 'Church Choir', 'yes', '1974-01-01', '1974-10-17', 'MID TAFO', 'JOHN ARJARQUAH ', 'OLD TAFO ZION PRIMARY', 244696016, 'married', 'Regular', 0),
(107, 'MC', 'STANLEY', 'ASAMOAH', '1984-09-23', 'male', 'Salaried -Govt', 553296111, 'TF 203 A ', 'NEAR ASSEMBLIES OF GOD CHURCH\r\nNEW TAFO-AKIM', 'no', 'Young Adults Fellowship', '---', 'yes', '2003-12-26', '2003-12-25', 'HSE. NO. TF 203 A NEW TAFO-AKIM E/R', 'MRS ASAMOAH A. JOYCE', 'ANYINASIN', 200757689, 'married', 'Regular', 0),
(108, 'MNC', 'EMMANUEL', 'LARWEH KWAKU', '1994-06-04', 'male', 'Student', 549853198, 'TH 25A', 'NELSBAN PALACE', 'yes', 'Young Peoples Guild', '---', 'no', '0011-01-01', '1000-01-01', 'TH 25A', 'KWAME ASARE', '--', 261986963, 'single', 'Regular', 0),
(109, 'MC', 'SIMON', 'COFFIE', '1972-03-31', 'male', 'Salaried -Govt', 546650211, 'TE 171', 'NANA HEMAA JUNCTION', 'no', 'Men Fellowship', '---', 'yes', '1000-01-01', '1000-01-01', '----', '---', 'ROSES SCHOOL', 0, 'single', 'Regular', 0);

-- --------------------------------------------------------

--
-- Table structure for table `occupation`
--

CREATE TABLE `occupation` (
  `id` int(11) NOT NULL,
  `occupation` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `occupation`
--

INSERT INTO `occupation` (`id`, `occupation`) VALUES
(1, 'Salaried -Govt'),
(2, 'Salaried -Private'),
(3, 'Student'),
(4, 'Non Salaried'),
(5, 'Unemployed'),
(6, 'Retired');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cgroups`
--
ALTER TABLE `cgroups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `children`
--
ALTER TABLE `children`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `junior`
--
ALTER TABLE `junior`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marital`
--
ALTER TABLE `marital`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `occupation`
--
ALTER TABLE `occupation`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cgroups`
--
ALTER TABLE `cgroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `children`
--
ALTER TABLE `children`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `junior`
--
ALTER TABLE `junior`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `marital`
--
ALTER TABLE `marital`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;
--
-- AUTO_INCREMENT for table `occupation`
--
ALTER TABLE `occupation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
