<?php
require_once 'core/init.php';
include 'includes/head.php';
 include 'includes/navigate.php';
 include 'helpers.php';

 ?>

<!-- church leaders -->
 <?php
   $display2 ="SELECT * FROM members WHERE church_leader like 'yes' AND deleted=0";
   $disp2=$db->query($display2);
   $count_com=mysqli_num_rows($disp2);
  ?>

<!-- non-communicants -->
  <?php
  $display3 ="SELECT * FROM members WHERE communicant like 'no'";
  $disp3=$db->query($display3);
 $count_ncom=mysqli_num_rows($disp3);
  ?>

  <!-- communicant -->
  <?php
    $display ="SELECT * FROM members WHERE communicant like 'yes' AND deleted=0";
    $disp=$db->query($display);
    $count1=mysqli_num_rows($disp);

   ?>
   <!--Children service  -->
  <?php
   $display ="SELECT * FROM children WHERE deleted =0";
   $disp=$db->query($display);
   $countC= mysqli_num_rows($disp);

  ?>
    <!-- Junior youth -->
 <?php
   $display ="SELECT * FROM junior WHERE deleted =0";
   $disp=$db->query($display);
   $countJ= mysqli_num_rows($disp);

  ?>


     <!-- youth People guild -->
     <?php
     $displayY ="SELECT * FROM members WHERE gen_group LIKE '%Guild'  AND deleted ='0'";
     $ypg=$db->query($displayY);
     $countY=mysqli_num_rows($ypg);
      ?>

      <!-- yaf -->
      <?php
      $displayYAF ="SELECT * FROM members WHERE gen_group LIKE '%Adult%'  AND deleted ='0'";
      $yafQ=$db->query($displayYAF);
      $count_yaf=mysqli_num_rows($yafQ);
       ?>


       <!-- Womens Fellowship -->
       <?php
       $displayW ="SELECT * FROM members WHERE gen_group LIKE 'Women%'  AND deleted ='0'";
       $womenQ=$db->query($displayW);
       $countW=mysqli_num_rows($womenQ);
        ?>


        <!-- mens Fellowship -->
        <?php
        $displayM ="SELECT * FROM members WHERE gen_group LIKE 'Men%'  AND deleted ='0'";
        $menQ=$db->query($displayM);
        $countM=mysqli_num_rows($menQ);
         ?>

<!--potential yaf members-->
<?php
$displayYAF ="SELECT * FROM members WHERE gen_group LIKE '%Potential YAF%'  AND deleted ='0'";
$yafQ=$db->query($displayYAF);
$countpPM=mysqli_num_rows($yafQ);
 ?>

<!--potential YPG-->
<?php
$displayY ="SELECT * FROM members WHERE gen_group LIKE '%Potential YPG'  AND deleted ='0'";
$ypg=$db->query($displayY);
$countPY=mysqli_num_rows($ypg);
 ?>
<!--potential women fellowship-->
<?php
$displayW ="SELECT * FROM members WHERE gen_group LIKE '%Potential WF%'  AND deleted ='0'";
$womenQ=$db->query($displayW);
$countPW=mysqli_num_rows($womenQ);
 ?>

<!--potential men feloowship-->
<?php
$displayM ="SELECT * FROM members WHERE gen_group LIKE 'Potential MF%'  AND deleted ='0'";
$menQ=$db->query($displayM);
$countPM=mysqli_num_rows($menQ);
 ?>

<!-- deleted members -->

<?php
  $display ="SELECT * FROM members WHERE deleted =1";
  $disp=$db->query($display);
  $del_count=mysqli_num_rows($disp);

 ?>
<!-- valid registered -->
 <?php
   $display1 ="SELECT * FROM members WHERE deleted =0";
   $disp1=$db->query($display1);
   $count_mem=mysqli_num_rows($disp1);
  ?>

<!--regular members-->
 <?php
   $display ="SELECT * FROM members WHERE deleted =0 AND membership LIKE 'Regular'";
   $disp=$db->query($display);
   $countR = mysqli_num_rows($disp);

  ?>
<!--distant members-->
<?php
   $display ="SELECT * FROM members WHERE deleted =0 AND membership LIKE 'Distant'";
   $disp=$db->query($display);
   $countD = mysqli_num_rows($disp);

  ?>
<!-- male-->
<?php
   $display ="SELECT * FROM members WHERE deleted =0 AND gender LIKE 'male'";
   $disp=$db->query($display);
   $count_male = mysqli_num_rows($disp);

  ?>
<!--female-->
<?php
   $display ="SELECT * FROM members WHERE deleted =0 AND gender LIKE 'Female'";
   $disp=$db->query($display);
   $count_female = mysqli_num_rows($disp);

  ?>
<!--unemployed-->
<?php
   $display ="SELECT * FROM members WHERE deleted =0 AND occupation LIKE 'Unemployed'";
   $disp=$db->query($display);
   $count_unemployed = mysqli_num_rows($disp);

  ?>
<!--private workers-->
<?php
   $display ="SELECT * FROM members WHERE deleted =0 AND occupation LIKE '%Private%'";
   $disp=$db->query($display);
   $count_private = mysqli_num_rows($disp);

  ?>

<!--Govt workers-->
<?php
   $display ="SELECT * FROM members WHERE deleted =0 AND occupation LIKE '%Govt%'";
   $disp=$db->query($display);
   $count_govt = mysqli_num_rows($disp);

  ?>
<!--students-->
<?php
   $display ="SELECT * FROM members WHERE deleted =0 AND occupation LIKE '%Student%'";
   $disp=$db->query($display);
   $count_students = mysqli_num_rows($disp);

  ?>
<!--retired workers-->
<?php
   $display ="SELECT * FROM members WHERE deleted =0 AND occupation LIKE '%Retired%'";
   $disp=$db->query($display);
   $count_retired = mysqli_num_rows($disp);

  ?>


 <br><br>
 <style media="screen">
  
.bold{
    font-size: 28px;
}

   b{
     color: red;
     font-size: 25px;
   }
 </style>

 <h1 class="text-center top"><strong>SUMMARY OF REGISTRATION</strong> </h1><hr>
<br><br>
 <div class="container-padded">
   <div class="summary">
     <div class="row">
         <div class="col-md-12">
<!-- first row-->
       <div class="col-md-4">
    <h4 class="bold"><strong>REGISTERED MEMBERS:</strong></h4><br>
    <h4 class="text-center"><b><?=$count_mem ?></b> </h4><br>
    <p></p>

    <h4 class="bold"> <strong>COMMUNICANT REGISTERED:</strong></h4><br>
    <h4 class="text-center"> <b><?=$count_com?></b></h4><br>
    <p></p>

    <h4 class="bold"> <strong>NON-COMMUNICANT REGISTERED:</strong></h4><br>
    <h4 class="text-center"> <b><?=$count_ncom ?></b> </h4><br>
    <p></p>

    <h4 class="bold"> <strong>REGULAR MEMBERS:</strong></h4><br>
    <h4 class="text-center"> <b><?=$countR ?></b> </h4><br>
    <p></p>

    <h4 class="bold"> <strong>DISTANT MEMBERS:</strong></h4><br>
    <h4 class="text-center"> <b><?=$countD ?></b> </h4><br>
    <p></p>

    <h4 class="bold"> <strong>YPG  REGISTERED:</strong></h4><br>
    <h4 class="text-center"> <b><?=$countY ?></b> </h4><br>
    <p></p>


    <h4 class="bold"> <strong>CHILDREN SERVICE REGISTERED:</strong></h4><br>
    <h4 class="text-center"> <b><?=$countC ?></b> </h4><br>
    <p></p>
           
     <h4 class="bold"> <strong>JUNIOR YOUTH REGISTERED:</strong></h4><br>
     <h4 class="text-center"> <b><?=$countJ ?></b></h4><br>
     <p></p>
           
    <h4 class="bold"> <strong>WOMEN FELLOWSHIP REGISTERED:</strong></h4><br>
    <h4 class="text-center"> <b><?=$countW ?></b> </h4><br>
    <p></p>
 </div>

<!-- second row-->

 <div class="col-md-4">

    <h4 class="bold"> <strong>YAF REGISTERED:</strong></h4><br>
    <h4 class="text-center"> <b><?=$count_yaf ?></b> </h4><br>
    <p></p>

    <h4 class="bold"> <strong>MEN FELLOWSHIP REGISTERED:</strong></h4><br>
    <h4 class="text-center"> <b><?=$countM ?></b> </h4><br>
    <p></p>

    <h4 class="bold"> <strong>POTENTIAL JY MEMBERS:</strong></h4><br>
    <h4 class="text-center"><b><?=$countPM ?></b></h4><br>
    <p></p>
     
    <h4 class="bold"> <strong>POTENTIAL YAF MEMBERS:</strong></h4><br>
    <h4 class="text-center"><b><?=$countPY ?></b></h4><br>
    <p></p>
     
    <h4 class="bold"> <strong>POTENTIAL MF MEMBERS:</strong></h4><br>
    <h4 class="text-center"><b><?=$countPM ?></b></h4><br>
    <p></p>
     
          
    <h4 class="bold"> <strong>POTENTIAL WF MEMBERS:</strong></h4><br>
    <h4 class="text-center"><b><?=$countPW ?></b></h4><br>
    <p></p>
     
     <h4 class="bold"> <strong>DISTANT MEMBERS:</strong></h4><br>
    <h4 class="text-center"><b><?=$countPW ?></b></h4><br>
    <p></p>
     
     <h4 class="bold"> <strong>REGULAR MEMBERS:</strong></h4><br>
    <h4 class="text-center"><b><?=$countPW ?></b></h4><br>
    <p></p>
    </div>
        
<!--        last row     -->
     <div class="col-md-4">
    <h4 class="bold"> <strong>MALE  REGISTERED:</strong></h4><br>
    <h4 class="text-center"> <b><?=$count_male ?></b> </h4><br>
    <p></p>

    <h4 class="bold"> <strong>FEMALE  REGISTERED:</strong></h4><br>
    <h4 class="text-center"> <b><?=$count_female ?></b> </h4><br>
    <p></p>

    <h4 class="bold"> <strong>UNEMPLOYED REGISTERED:</strong></h4><br>
    <h4 class="text-center"><b><?=$count_unemployed ?></b></h4><br>
    <p></p>
     
    <h4 class="bold"> <strong>GOVERNMENT WORKERS:</strong></h4><br>
    <h4 class="text-center"><b><?=$count_govt ?></b></h4><br>
    <p></p>
     
    <h4 class="bold"> <strong>PRIVATE WORKERS:</strong></h4><br>
    <h4 class="text-center"><b><?=$count_private ?></b></h4><br>
    <p></p>
     
          
    <h4 class="bold"> <strong>RETIRED WORKERS:</strong></h4><br>
    <h4 class="text-center"><b><?=$count_retired ?></b></h4><br>
    <p></p>
     
     <h4 class="bold"> <strong>STUDENTS REGISTERED:</strong></h4><br>
    <h4 class="text-center"><b><?=$count_students ?></b></h4><br>
    <p></p>
     
     <h4 class="bold"> <strong>REGULAR MEMBERS:</strong></h4><br>
    <h4 class="text-center"><b><?=$countPW ?></b></h4><br>
    <p></p>
    </div>
             
     </div>
    </div>
   </div>

 </div>
