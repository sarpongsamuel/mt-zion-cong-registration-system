<?php
require_once 'core/init.php';
include 'includes/head.php';
 include 'includes/navigate.php';
 include '../helpers.php';

 ?>
 <?php

 if (isset($_GET['restore']) && !empty($_GET['restore'])) {
   $res_id=(int)$_GET['restore'];
   $res_id=sanitize($res_id);
   $sql="UPDATE junior set deleted = 0  WHERE id='$res_id'";
   $db->query($sql);
   header('Location: view.php');
 }

 if (isset($_GET['remove']) && !empty($_GET['remove'])) {
   $del_id=(int)$_GET['remove'];
   $del_id=sanitize($del_id);
   $sql1="DELETE  FROM junior  WHERE id='$del_id'";
   $db->query($sql1);
   // header('Location: view.php');
 }


  ?>
<div class="container">


 <h3 class="text-center top">Archived Members</h3>
 <a href="index.php" class="btn btn-primary pull-right" id="add-product-btn">Register New Member</a><div class="clearfix"></div>

 <div class="clearfix"></div>
 <hr>
 </div>
 <?php
   $display ="SELECT * FROM junior WHERE deleted =1";
   $disp=$db->query($display);

  ?>

  <table class="table table-bordered table-condensed table-striped">
     <thead><th></th><th>ID</th><th>Firstname</th><th>Lastname</th><th>Date Of Birth</th><th>Gender</th><th>Telephone</th><th>Parent/Guardian name</th><th>Parent/Guardian Phone</th><th>House Number</th><th>Relation</th><th>Address</th>
       <th>Baptism Date</th><th>Confirmation Date</th></thead>
     <tbody>
       <?php while($view =mysqli_fetch_assoc($disp)): ?>
         <tr>
           <td>
            <a href="archive.php?restore=<?=$view['id'] ?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-refresh"></span></a>
            <a href="archive.php?remove=<?=$view['id'] ?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-remove"></span></a>
           </td>
           <td><?=$view['id']; ?></td>
           <td><?=$view['firstname'] ?></td>
           <td><?=$view['lastname'] ?></td>
           <td><?=$view['bdate'] ?></td>
           <td><?=$view['gender'] ?></td>
           <td><?=$view['telephone'] ?></td>
           <td><?=$view['P_name'] ?></td>
           <td><?=$view['P_phone'] ?></td>
           <td><?=$view['house_no'] ?></td>
           <td><?=$view['relation'] ?></td>
           <td><?=$view['address'] ?></td>
           <td><?=$view['baptism_date'] ?></td>
           <td><?=$view['confirmation_date'] ?></td>
         <?php endwhile; ?>
     </tbody>
 </table



 <?php include 'includes/footer.php'; ?>
