<?php
require_once 'core/init.php';
include 'includes/head.php';
include 'includes/navigate.php';
 ?>
 <style type="text/css">
 	h1{
 		text-transform: uppercase;
 		font-family: sans-serif;
 		margin-top: 40px;
 	}
 </style>
 <h1 class="text-center top">Junior Youth Registration</h1><hr><br><br><br>
 <form class="" action="verify.php" method="post" enctype="multipart/form-data">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4 col-md-offset-2">
        <label for=""><h4>firstname</h4></label>
        <input type="text" class="form-control" name="firstname" value="" placeholder="firstname">
      </div>
      <div class="col-md-4 col-md-offset-1">
        <label for=""><h4>Lastname</h4></label>
        <input type="text" name="lastname" class="form-control" value="" placeholder="lastname">
      </div>
    </div>
    <br>

      <div class="row">
      <div class="col-md-4 col-md-offset-2">
        <label for=""><h4>Date Of Birth</h4></label>
        <input type="date" name="bdate" class="form-control" value="">
      </div>
    <div class="col-md-4 col-md-offset-1">
    <label for=""><h4>Age</h4></label>
    <input type="number" name="age" class="form-control" value="">
    </div>
      
      </div>
      <br>
      
      <div class="row">
     <div class="col-md-4 col-md-offset-2">
        <label for=""><h4>Gender</h4></label>
        <select class="form-control" name="gender" value="">
          <option value="male">Male</option>
          <option value="Female">Female</option>
        </select>
      </div>
      <div class="col-md-4 col-md-offset-1">
     <label for=""><h4>Personal Contact</h4></label>
    <input type="text" name="telephone" class="form-control" value="" >
     </div>
     </div>
      <br>
      
    <div class="row">
       <div class="col-md-4 col-md-offset-2">
         <label for=""><h4>Are You Baptised ?</h4></label>
         <select class="form-control" name="ver_bapt">
             <option value="Yes">Yes</option>
             <option value="No">No</option>
        </select>
       </div>
      <div class="col-md-4 col-md-offset-1">
        <label for=""><h4>Are You Confirmed</h4></label>
          <select class="form-control" name="ver_confirm">
             <option value="Yes">Yes</option>
             <option value="No">No</option>
        </select>
     </div>
   </div>
<br> 


    <div class="row ">
       <div class="col-md-4 col-md-offset-2">
         <label for=""><h4>Baptism Date</h4></label>
         <input type="text" name="baptism" class="form-control" value="">
       </div>
      <div class="col-md-4 col-md-offset-1">
        <label for=""><h4>Confirmation Date</h4></label>
        <input type="text" name="confirmation" class="form-control" value="" placeholder="">
     </div>
   </div>
<br> 
    

    <div class="row">
       <div class="col-md-4 col-md-offset-2">
         <label for=""><h4>School</h4></label>
         <input type="text" name="school" class="form-control" value="">
       </div>
      <div class="col-md-4 col-md-offset-1">
        <label for=""><h4>Academic Level</h4></label>
        <select class="form-control" name="aca_level">
          <option value="primary">Primary</option>
          <option value="JHS">Junior High</option>
          <option value="SHS">Senior High</option>
          <option value="SHS_GRADUATE">Senior High Graduate</option>
        </select>
     </div>
   </div>
<br>

      <div class="row">
      	<div class="col-md-4 col-md-offset-2">
      <label for=""><h4>Parent/Guardian Name</h4></label>
      <input type="text" name="parent_name" class="form-control" value="" placeholder="">
    </div>
      <div class="col-md-4 col-md-offset-1">
      <label for=""><h4>Parent/Guardian Telephone</h4></label>
      <input type="text" name="parent_phone" class="form-control" value="" placeholder="0XXXXXXXXX">
    </div>
  </div>
  <br>
  
  <div class="row">
	<div class="col-md-4 col-md-offset-2">
    <label for=""><h4>Relation To Member</h4></label>
    <select name="relation" class="form-control">
    	<option value="Parent">Parent</option>
    	<option value="Guardian">Guardian</option>
    </select>
  </div>
<div class="col-md-4 col-md-offset-1">
    <label for=""><h4>Is Parent/Guardian a Member Of This Church ?</h4></label>
    <select class="form-control" name="parent_church">
        <option value="Yes">Yes</option>
        <option value="No">No</option>
    </select>
  </div>
</div>
<br>
  
<div class="row">	
<div class="col-md-4 col-md-offset-2">
  <label for=""><h4>Nearest Landmark/Area/house#</h4></label>
<textarea name="address" rows="4" cols="10" class="form-control" placeholder=""></textarea>
</div>
<div class="col-md-4 col-md-offset-1">
    <label for=""><h4>House Number</h4></label>
    <input type="text" name="house" class="form-control" value="" placeholder="TE XXX">
  </div>
</div>
      
 
      
<hr>
<!-- other details -->
<?php

?>
    <div class="row">
      <input type="submit" name="submit"  class="btn btn-primary btn-md col-md-offset-5  complete" value="Register member">
      <a href="../members.php" class="btn btn-warning btn-md">Cancel</a>
    </div>
  </div>
</form>
<br>
<?php include "../includes/footer.php"?>
