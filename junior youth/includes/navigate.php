<nav class="navbar navbar-inverse probootstrap-navbar ">
   <div class="container">
   <div class="navbar-header">
<div class="btn-more js-btn-more visible-xs">
      <a href="#"><i class="icon-dots-three-vertical "></i></a>
</div>
<button type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false" aria-controls="navbar">
       <span class="sr-only">Toggle navigation</span>
       <span class="icon-bar"></span>
       <span class="icon-bar"></span>
       <span class="icon-bar"></span>
</button>

<a class="navbar-brand" href="index.php" title="ProBootstrap:Enlight">Mountzion Members Database~<small>Junior Youth</small></a>
</div>
<div id="navbar-collapse" class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="../index.php">Dashboard</a></li>
            <li><a href="view.php">Edit Registered</a></li>
            <!-- <li><a href="archive.php">View Registered</a></li> -->
            <li><a href="members.php">Registered</a></li> 
            <li><a href="archive.php">Archived Members</a></li>
            <li class="dropdown">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Category<span class="caret"></span></a>
                 <ul class="dropdown-menu">
                   <li><a href="upper_jy.php">Upper Junior Youth</a></li>
                   <li><a href="lower_jy.php">Lower Junior Youth</a></li>
                   <li><a href="confirmed.php">JY Confirmed</a></li>
                   <li><a href="baptised.php">JY Baptised</a></li>
                     <li><a href="pot_ypg.php">Graduates</a></li>
                 </ul>
             </li>
<!--            category-->
               <li class="dropdown">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Academic Levels<span class="caret"></span></a>
                 <ul class="dropdown-menu">
                   <li><a href="primary.php">Primary School</a></li>
                   <li><a href="jhs.php">Junior High School</a></li>
                   <li><a href="shs.php">Senior High School</a></li>
                   <li><a href="graduate.php">SHS Graduate</a></li>
                 </ul>
             </li> 
<!--            academic levels-->
            
             <!-- <li><a href="../statictics.php">Reg. Summary</a></li> -->
          </ul>
</div>
</div>
</nav>
