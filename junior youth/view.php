<?php
require_once 'core/init.php';
include 'includes/head.php';
 include 'includes/navigate.php';
 include '../helpers.php';

 ?>
 <?php

 if (isset($_GET['delete']) && !empty($_GET['delete'])) {
   $delete_id=(int)$_GET['delete'];
   $delete_id=sanitize($delete_id);
   $sql="UPDATE junior set deleted = 1  WHERE id='$delete_id'";
   $db->query($sql);
   header('Location: view.php');
 }

  ?>
<div class="container">


 <h2 class="text-center top">Registered Members</h2>
 <a href="index.php" class="btn btn-primary pull-right" id="add-product-btn">Register New Member</a><div class="clearfix"></div>
 <a href="archive.php" class="btn btn-warning pull-left" id="restore">Archived Members</a>
 <div class="clearfix"></div>
 <hr>
 </div>
 <?php
   $display ="SELECT * FROM junior WHERE deleted =0";
   $disp=$db->query($display);
   $count= mysqli_num_rows($disp);

  ?>

<h3 class="text-center ">Now Showing --<?=$count ?></h3><br><br><br>
 <table class="table table-bordered table-condensed table-striped">
     <thead><th></th><th>ID</th><th>Firstname</th><th>Lastname</th><th>Date Of Birth</th><th>Gender</th><th>Telephone</th><th>Parent/Guardian name</th><th>Parent/Guardian Phone</th><th>School</th><th>Academic Level</th><th>House Number</th><th>Relation</th><th>Address</th>
       <th>Baptism Date</th><th>Confirmation Date</th></thead>
     <tbody>
       <?php while($view =mysqli_fetch_assoc($disp)): ?>
         <tr>
           <td>
             <a href="update.php?edit=<?=$view['id'] ?>" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-pencil"></span></a>
             <a href="view.php?delete=<?=$view['id'] ?>" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
           </td>
           <td><?=$view['id']; ?></td>
           <td><?=$view['firstname'] ?></td>
           <td><?=$view['lastname'] ?></td>
           <td><?=$view['bdate'] ?></td>
           <td><?=$view['gender'] ?></td>
           <td><?=$view['telephone'] ?></td>
           <td><?=$view['P_name'] ?></td>
           <td><?=$view['P_phone'] ?></td>
           <td><?=$view['school'] ?></td>
           <td><?=$view['academic'] ?></td>
           <td><?=$view['house_no'] ?></td>
           <td><?=$view['relation'] ?></td>
           <td><?=$view['address'] ?></td>
           <td><?=$view['baptism_date'] ?></td>
           <td><?=$view['confirmation_date'] ?></td>
         <?php endwhile; ?>
     </tbody>
 </table



 <?php include 'includes/footer.php'; ?>
