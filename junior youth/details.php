<?php
require_once '../core/init.php';
include 'includes/head.php';
 include 'includes/navigate.php';
 include 'helpers/helpers.php';

?>
<?php
  if (isset($_GET['view']) && !empty($_GET['view'])) {
    $show_id=(int)$_GET['view'];
    $show_id=sanitize($show_id);

   $show = "SELECT * FROM junior WHERE id ='$show_id'";
   $showRes = $db->query($show);
   $showQ= mysqli_fetch_assoc($showRes);

}

 ?>

<h1 class="text-center top">Member Details</h1><hr>
 <div class="container display">
   <div class="row">
   <div class="col-md-12">
     <div class="col-md-3">
       <label><h3>Member Id*</h3></label><br>
       <h4  class="count1"><?=$showQ['id']?></h4>
     </div>

     <div class="col-md-3 col-md-ofset-3 ">
       <label><h3>Firstname*</h3></label><br>
       <h4  class="count1"><?=$showQ['firstname']; ?></h4>
     </div>

     <div class="col-md-3 ">
       <label><h3>Lastname*</h3></label><br>
       <h4  class="count1"><?=$showQ['lastname']; ?></h4>
     </div>

     <div class="col-md-3  ">
       <label><h3>Gender*</h3></label><br>
       <h4  class="count1"><?=$showQ['gender']; ?></h4>
     </div>
   </div>
 </div>
 <br>

<div class="row">

   <div class="col-md-12">
     <div class="col-md-3 col-md-ofset-3 ">
       <label><h3>Date of Birth*</h3></label><br>
       <h4  class="count1"><?=$showQ['bdate']; ?></h4>
     </div>

     <div class="col-md-3 ">
       <label><h3>Age*</h3></label><br>
       <h4  class="count1"><?=$showQ['age']; ?></h4>
     </div>

     <div class="col-md-3">
       <label><h3>Academic Level</h3></label><br>
       <h4  class="count1"><?=$showQ['academic']; ?></h4>
     </div>
     <div class="col-md-3 ">
       <label><h3>Nearest Landmark*</h3></label><br>
       <h4  class="count1"><?=$showQ['address']; ?></h4>
     </div>
  <div class="clearfix"></div>
   </div>
 </div>
<br>

 <div class="row">
    <div class="col-md-12">
      <div class="col-md-3 col-md-ofset-3 ">
        <label><h3>Name of School*</h3></label><br>
        <h4  class="count1"><?=$showQ['school']; ?></h4>
      </div>
      <div class="col-md-3 col-md-ofset-3 ">
        <label><h3>House Number*</h3></label><br>
        <h4  class="count1"><?=$showQ['house_no']; ?></h4>
      </div>

      <div class="col-md-3">
        <label><h3>Baptism Date*</h3></label><br>
        <h4 class="count1"><?=$showQ['baptism_date']; ?></h4>
      </div>

         <div class="col-md-3">
         <label><h3>Confirmation Date*</h3></label><br>
         <h4 class="count1"><?=$showQ['confirmation_date']; ?></h4>
       </div>
    </div>
  </div>
<br>
  <div class="row">
     <div class="col-md-12">
       <div class="col-md-3">
         <label><h3>Telephone*</h3></label><br>
         <h4 class="count1"><?=$showQ['telephone']; ?></h4>
       </div>
       <div class="col-md-3">
         <label><h3>Parent/Guardian Name*</h3></label><br>
         <h4 class="count1"><?=$showQ['P_name']; ?></h4>
       </div>

       <div class="col-md-3 ">
         <label><h3>Parent/Guardian Contact*</h3></label><br>
         <h4 class="count1"><?=$showQ['P_phone']; ?></h4>
       </div>
       <div class="col-md-3 ">
         <label><h3>Relation To Member</h3></label><br>
         <h4 class="count1"><?=$showQ['relation']; ?></h4>
       </div>

     </div>
   </div>
<br>

<br><br>
   <a href="index.php" class="btn btn-primary  btn-md pull-right">>>Home</a>
 </div>
 <?php include 'includes/footer.php'; ?>
